<?php
class Adodis_Featuredproducts_Model_Adminhtml_System_Config_Source_Animationtype
{

   public function toOptionArray()
    {
        return array(
            array('value' => 'slow', 'label'=>Mage::helper('adminhtml')->__('Slow')),
             array('value' => 'fast', 'label'=>Mage::helper('adminhtml')->__('Fast')),
        );
    }

}