var td_WVTAV_product = "td_WVTAV_product_";
var WVTAV_td_Price = "WVTAV_td_totalPrice";
var WVTAV_sp_price = "WVTAV_sp_price";
var WVTAV_related_products = "WVTAV_related_products_";

function WVTAV_UpdatePrice(pr_arr, ele)
{
	var _related_product = WVTAV_related_products + ele;
	if($(_related_product).checked)
	{		
		$(WVTAV_sp_price).innerHTML = (parseFloat($(WVTAV_sp_price).innerHTML) + parseFloat(pr_arr[ele])).toFixed(2);
		startAnimation_WVTAV(td_WVTAV_product + ele, "");
	}
	else
	{
		$(WVTAV_sp_price).innerHTML = (parseFloat($(WVTAV_sp_price).innerHTML) - parseFloat(pr_arr[ele])).toFixed(2);
		startAnimation_WVTAV(td_WVTAV_product + ele, "fade");
	}
	if(isUseWVTAVAnim)startAnimation_WVTAV(WVTAV_td_Price, "");
}

function startAnimation_WVTAV(cont, type)
{
		if(type == "fade")
		{
			if(isUseWVTAVAnim)
			$(cont).fade();
			else
			$(cont).hide();
		}
		else
		{
			if(isUseWVTAVAnim)
			{
				$(cont).setOpacity(0);
				$(cont).setStyle({display: 'block'});
				new Effect.Opacity(
				   cont, { 
					  from: 0.0, 
					  to: 1.0,
					  duration: 2.0
				   }
				);
				$(cont).setStyle({display: ''});
			}
			else
			{
				$(cont).show();
			}
		}
}

function bt_WVTAV_addToCart() {	
	var checkboxes = $$('.WVTAV_related_checkbox');
	var values = [];
	for (var i = 0; i < checkboxes.length; i++)
	{
		 if(checkboxes[i].checked)
		 {
			 values.push(checkboxes[i].value);
		 }
	}
	var primaryProductID = values.shift();
	var s = productAddToCartForm.form.action;
	s = s.substr(0, s.lastIndexOf('/') - 1);
	productAddToCartForm.form.action = s.substr(0, s.lastIndexOf('/')) + '/' + primaryProductID + '/';
	productAddToCartForm.form.product.value = primaryProductID;
	if ($('related-products-field')) $('related-products-field').value = values.join(',');
	productAddToCartForm.submit();
}