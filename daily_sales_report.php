<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require 'app/Mage.php';
Mage::app(); 
// Create order collection object
$collection = Mage::getModel('sales/order')->getCollection();
$curdatetoemail=date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
$curdate=date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));
// Apply date filter. This would be 1 day in production but using this range as a test.
/*
$frcurdate='2012-05-01';
$collection->addFieldToFilter(
 'created_at', 
 array('from' => $frcurdate, 'to' => $curdate)
);

*/


$collection->addFieldToFilter(
 'created_at', 
 array('from' => $curdate)
);

$records=array();
// Iterate it for displaying results
foreach ($collection as $order) {
 $records[]=order_items($order->increment_id);
}
$file_name='daily_sales.csv';
$file_tosave='dailyreport/'.$file_name;
//$file_tosave=Mage::getBaseDir('dailyreport').'\\'.$file_tosave_name;
save_incsv($file_tosave,$records); // generate and save csv file

// send email //


try{
$mail = new Zend_Mail();
$mail->setFrom("ikoala@gmail.com","ikoala");
$mail->addTo("mahesh@tisindiasupport.com","mahesh");
$mail->setSubject("Ikoala Total Sales on :- ".$curdatetoemail);
$mail->setBodyHtml(" daily report file attached  with this email"); // here u also use setBodyText options.
 
// this is for to set the file format
$fileContents = file_get_contents($file_tosave); 
$at = new Zend_Mime_Part($fileContents);
$at->type = 'application/csv'; // if u have PDF then it would like -> 'application/pdf'
$at->disposition = Zend_Mime::DISPOSITION_INLINE;
$at->encoding = Zend_Mime::ENCODING_8BIT;
$at->filename = $file_name;
$mail->addAttachment($at);
$mail->send();
 
}catch(Exception $e)
{
echo $e->getMassage();
 
}

function order_items($orderNumber)
{
$products_rows = array(); 
$order = Mage::getModel('sales/order')->loadByIncrementId($orderNumber);
// get order total value
$order_date = date("d/m/Y",Mage::getModel('core/date')->timestamp($order->created_at));
$store_name = $order->store_name;
$shipping_description=$order->shipping_description;
$customer_id=$order->customer_id;
$subtotal_incl_tax=$order->subtotal_incl_tax;
$shipping_address_id=$order->shipping_address_id;
$total_qty_ordered=$order->total_qty_ordered;
$store_currency_code=$order->store_currency_code;
$grand_total=$order->grand_total;
$shipping_amount=$order->shipping_amount;
$orderValue = number_format ($order->getGrandTotal(), 2, '.' , $thousands_sep = '');
// get shipping addresss
 $shippingaddress = Mage::getModel('sales/order_address')->load($shipping_address_id);
 $address=$shippingaddress->getData();
//print_r($address);
$firstname=$address['firstname'];
$lastname=$address['lastname'];
$street=$address['street'];
$city=$address['city'];
$state=$address['region'];
$postcode=$address['postcode'];
$telephone=$address['telephone'];
$email=$address['email'];

/*
// get  customer details
$customer = Mage::getModel('customer/customer')->load($customer_id);
echo "<b><br>".$customerFirstName = $customer->getFirstname();
echo  "<br>".$email = $customer->getEmail(). "<br>------------------------------</b><br>";

echo "shipping_amount : ".$shipping_amount;
echo "<br>subtotal_incl_tax : ".$subtotal_incl_tax;
//echo "<br>subtotal_incl_tax : ".$subtotal_incl_tax;
//echo "<br>subtotal_incl_tax : ".$subtotal_incl_tax;
echo "<br>store_currency_code : ".$store_currency_code;

echo "<br>Shipping Address:- ".$street. ",<br>City : ".$city. "<br>state : ".$state. "<br>postcode : ".$postcode. "<br> telephone : ".$telephone. "<br>email:".$email. "<br>";
*/

// get order item collection
$orderItems = $order->getAllItems(); 
//$orderItems = $order->getItemsCollection();
$p_sk="";
foreach ($orderItems as $item){
    $product_id = $item->product_id;
    $product_sku = $item->sku;
    $product_name = $item->getName();
	$product_price = $item->getPrice();
	$qty_ordered= ceil($item->qty_ordered);
	$product_price = number_format ($product_price, 2, '.' , $thousands_sep = '');	
	 $store_name = $store_name;	
	 if($product_sku!=$p_sk)
	{
    $_product = Mage::getModel('catalog/product')->load($product_id);
	$ordered_options="";
	$productOptions = $item->getProductOptions();
    if (isset($productOptions['attributes_info'])) {
      foreach ($productOptions['attributes_info'] as $productOption) {
        $ordered_options .= $productOption['label']." : ".$productOption['value'].","; 
         }  
	  }
		 $ordered_options=rtrim($ordered_options,",");  
		 $data = array();	
		 $data['dealNumb'] = $dealNumb; // need to fetch from db
		$data['orderNumber'] = $orderNumber; 
		$data['redemptionCode'] = $redemptionCode; // need to fetch from db
		$data['store_name'] = $store_name;        
		$data['distributor'] = $_product->distributor;
		$data['product_name'] = $product_name;
		$data['sku'] = $product_sku;
		$data['product_price'] = $product_price;
		$data['store_currency_code'] = $store_currency_code;
		$data['qty_ordered'] = $qty_ordered;
		$data['product_options'] = $ordered_options;
		$data['firstname'] = $firstname;
        $data['lastname'] = $lastname;
        $data['email'] = $email;
		$data['street'] = $street;
		$data['city'] = $city;
		$data['region'] = $state;
		$data['postcode'] = $postcode;
		$data['telephone'] = $telephone;	
		//$data['shipping_amount'] = $shipping_amount;	
		$data['shipping_description'] = $shipping_description;	
		
		$data['order_date'] =$order_date;
		$products_rows[] = $data;

	}
$p_sk=$product_sku;
}

return $products_rows;
}

function save_incsv($file_path,$data)
{
	$mage_csv = new Varien_File_Csv(); //mage CSV
    $products_row = array();   
	$heading_row= array("Deal ID","Order Number","Redemption Code","Store/website","Distributor","Product Name","Sku","Price","Currency code","Quantity Ordered","Product Options","Customer First Name","Customer Last Name","Customer Street","Customer Suburb","Customer State","Customer Postcode","Customer Telephone","Customer Email","Shipping description","Order Date");

	$products_row[]=$heading_row; 
	foreach($data as $valrow)
	{
		if(count($valrow)>0)
		{
			foreach($valrow as $row)
			{
			$products_row[] = $row;
			}
		}
		else
		{
			 $products_row[] = $valrow;
		}
	}
   
       //write to csv file
   $mage_csv->saveData($file_path, $products_row); //note $products_row will be two dimensional array
 
 echo "report file saved";
}
?>