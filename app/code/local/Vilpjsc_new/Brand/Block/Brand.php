<?php

class Vilpjsc_Brand_Block_Brand extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    //Get Collection from Brand Controller
    public function getBrand() {
        if (!$this->hasData('brand')) {
            $this->setData('brand', Mage::registry('brand'));
        }
        return $this->getData('brand');
    }

    //Get Brand Featured Collection
    public function getFeaturedBrand() {
        return $this->getBrand()->getCollection()->setPageSize(12)->addFilter('status', '1');
    }

    public function resizeBrand($img,$width=100, $height=60) {
        if (!file_exists("./media/brand/resized"))
            mkdir("./media/brand/resized", 0777);
        $imageResized = Mage::getBaseDir('media') . DS . "brand" . DS . "resized" . DS . $width."x".$height.$img;
        if (!file_exists($imageResized) && file_exists("./media/brand/" . $img)) {
            $imageObj = new Varien_Image("./media/brand/" . $img);
            $imageObj->constrainOnly(TRUE);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->keepFrame(FALSE);
            $imageObj->resize($width, $height);
            $imageObj->save($imageResized);
        }
        $newImageUrl = Mage::getBaseUrl('media')."brand/resized/".$width."x".$height.$img;
        return $newImageUrl;
    }

    public function getNormalBrand() {
        return $this->getBrand()->getCollection()->addFilter('status', '0');
    }

    public function getBrandChar() {
        extract($this->getBrandKey());
        return $this->getBrand()->getCollection()->saveBrandcollection($char);
    }

    //Create Alphabet navigation
    public function navigation() {
        extract($this->getBrandKey());
        $alphas = range('A', 'Z');
        $navigation = "";
        $digitClass = "";
        foreach ($alphas as $key) {
            if ($char == $key)
                $class_key = "current_char";
            else
                $class_key="";
            $navigation .= "<li class='key_item " . $class_key . "'><a href='" . Mage::getBaseUrl() . "brand/index/index/brand_key/" . $key . "'>" . $key . "</a></li>";
        }
        if ($char == "digit") {
            $digitClass = "current_char";
        } elseif ($char == "all")
            $allClass = "current_char";
        $navigation = "<li class='key_item " . $digitClass . "'><a href='" . Mage::getBaseUrl() . "brand/index/index/brand_key/digit'>#</a></li>" .
                $navigation;
        return $navigation;
    }

    //Get request for brand block
    public function getBrandKey() {
        $brand_key = Mage::registry('brand_key');

        if ($brand_key) {
            $char = $brand_key->getParam('brand_key') ? $brand_key->getParam('brand_key') : 'A';
            $option_id = $brand_key->getParam('option_id') ? $brand_key->getParam('option_id') : '1';
            $brand = $brand_key->getParam('brand') ? $brand_key->getParam('brand') : 'Nike';
        } else {
            $char = 'digit';
            $option_id = '544';
        }
        return compact('char', 'option_id', 'brand');
    }

}