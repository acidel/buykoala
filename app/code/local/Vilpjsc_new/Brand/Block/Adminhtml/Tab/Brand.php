<?php

/**
 * Magento Commercial Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Commercial Edition License
 * that is available at: http://www.magentocommerce.com/license/commercial-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/commercial-edition
 */

/**
 * Product in category grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Vilpjsc_Brand_Block_Adminhtml_Tab_Brand extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('catalog_category_products');
        $this->setDefaultSort('value');
        $this->setDefaultLimit(300);
        $this->setUseAjax(true);
    }

    public function getCategory() {
        return Mage::registry('category');
    }

    protected function _addColumnFilterToCollection($column) {
        // Set custom filter for in category flag
        if ($column->getId() == 'in_category') {
            $brandIds = $this->_getSelectedBrands();
            if (empty($brandIds)) {
                $brandIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $brandIds));
            } elseif (!empty($brandIds)) {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $brandIds));
            }
        } elseif($column->getId() == 'title'){
            $this->getCollection()->addFieldToFilter('store_value.value', array('like' => $column->getFilter()->getValue()."%"));
        }else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection() {
        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setCodeFilter('manufacturer')->getFirstItem();
        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                        ->setPositionOrder('asc')
                        ->setAttributeFilter($attributeInfo->getAttributeId())
                        ->setStoreFilter(Mage::app()->getStore()->getId());
        /* $collection = Mage::getModel('brand/brand')->getCollection(); */
        $collection->getSelect()->joinLeft(
                array('brand' => $collection->getTable('brand/brand')),
                'brand.manufacturer_option_id = main_table.option_id',
                array('brand.status as brand_status', 'brand.filename as logo', 'brand.content as brand_content')
        );
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('in_category', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => $this->_getName(),
            'values' => $this->_getSelectedBrands(),
            'align' => 'center',
            'index' => 'option_id',
            'renderer' => new Vilpjsc_Brand_Block_Adminhtml_Tab_Brand_Renderer_Checkbox()
        ));
        $this->addColumn('title', array(
            'header_css_class' => 'a-center',
            'header' => Mage::helper('brand')->__('Brand Name'),
            'width' => '120',
            'index' => 'value',
            'filter_index' => 'store_value',
        ));
        $this->addColumn('content', array(
            'header_css_class' => 'a-center',
            'header' => Mage::helper('brand')->__('Descrition'),
            'sortable' => true,
            'index' => 'brand_content'
        ));
        $this->addColumn('logo', array(
            'header_css_class' => 'a-center',
            'header' => Mage::helper('brand')->__('Logo'),
            'sortable' => true,
            'width' => '120',
            'align' => 'center',
            'index' => 'logo',
            'renderer' => new Vilpjsc_Brand_Block_Adminhtml_Brand_Grid_Renderer_Image()
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _getSelectedBrands() {
        $brands = $this->getRequest()->getPost('selected_brands');
        $categoryId = $this->getCategory()->getId();
        if (is_null($brands)) {
            $arrayBrands = array();
            $brands = Mage::getModel("brand/category")->getCollection()->addFieldToFilter("entity_id", array("eq" => $categoryId));
            foreach ($brands as $brand) {
                $arrayBrands[] = $brand->getBrandId();
            }
            return $arrayBrands;
        }
        else
            return $brands;
    }

    protected function _getName() {
        return "brand[id][]";
    }

}

