<?php
class Vilpjsc_Brand_Block_Adminhtml_Tab_Brand_Renderer_Checkbox extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Checkbox
{
    protected function _getCheckboxHtml($value, $checked){
        return '<input type="checkbox" name="brands[]" value="' . $value . '" class="'. ($this->getColumn()->getInlineCss() ? $this->getColumn()->getInlineCss() : 'checkbox' ).'"'.$checked.'/>';
    }

}
