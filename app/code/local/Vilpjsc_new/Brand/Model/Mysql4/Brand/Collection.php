<?php

/**
 * Vilpjsc Brand Status Model
 *
 * @category   Vilpjsc
 * @package    Vilpjsc_Brand
 * @author     An <an@Vilpjscusa.com>
 */
class Vilpjsc_Brand_Model_Mysql4_Brand_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    private $collect;

    public function _construct() {
        parent::_construct();
        $this->_init('brand/brand');
    }

    public function saveBrandcollection($char = "all") {

        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setCodeFilter('manufacturer')->getFirstItem();
        $collect = Mage::getResourceModel('eav/entity_attribute_option_collection')
                        ->setPositionOrder('asc')
                        ->setAttributeFilter($attributeInfo->getAttributeId())
                        ->setStoreFilter(Mage::app()->getStore()->getId());
        if ($char == "digit") {
            $query = "value REGEXP ?";
            $key = "^[0-9]";
            $collect->getSelect()->having($query, $key);
        } else {
            $query = "value like ?";
            $key = $char . '%';
            $collect->getSelect()->having($query, $key);
        }
        return $collect;
    }

}