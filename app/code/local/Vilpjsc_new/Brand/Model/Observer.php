<?php

class Vilpjsc_Brand_Model_Observer extends Mage_Core_Model_Abstract {

    public function saveCategory($observer) {
        $event = $observer->getEvent();
        $brands = $observer->getEvent()->getRequest()->getPost('brands');
        $categoryId = $event->getCategory()->getId();
        $model = Mage::getModel("brand/category")->load($categoryId, "entity_id");
        $collection = $model->getCollection()->addFieldToFilter("entity_id", array("eq" => $categoryId));
        $arrayBrands = array();
        foreach ($collection as $bcate) {
            $arrayBrands[] = $bcate->getBrandId();
        }
        if ($brands != NULL) {
            $delete = array_diff($arrayBrands, $brands);
            $save = array_diff($brands, $arrayBrands);
            foreach ($save as $item) {

                try {
                    $newmodel = Mage::getModel("brand/category");
                    $newmodel->setBrandId($item)->setEntityId($categoryId)->save();
                } catch (Exception $e) {
                    
                }
            }
            foreach ($delete as $item) {
                try {
                    $newmodel = Mage::getModel("brand/category")->load($categoryId, "entity_id");
                    $newmodel->load($item, "brand_id")->delete();
                } catch (Exception $e) {

                }
            }
        } else {
            foreach ($arrayBrands as $item) {

                try {
                    $model->load($item, "brand_id")->delete();
                } catch (Exception $e) {

                }
            }
        }
    }

}
