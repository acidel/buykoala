<?php

/**
 * Vilpjsc Brand
 *
 * @category   Vilpjsc
 * @package    Vilpjsc_Brand
 * @author     An <paul.aan@facebook.com>
 */
class Vilpjsc_Brand_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getBrandofProduct($_product) {
        $attribute = $_product->getResource()->getAttribute("manufacturer");
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            if (is_numeric($options[0]['value']))
                $brand = Mage::getModel("brand/brand")->load($options[0]['value'], 'manufacturer_option_id');
            if ($brand != NULL)
                return $brand;
        }
    }

    public function getbrandConfig() {
        $small['width'] = Mage::getStoreConfig('vilpbrand/small/width');
        $small['height'] = Mage::getStoreConfig('vilpbrand/small/height');
        $medium['width'] = Mage::getStoreConfig('vilpbrand/medium/width');
        $medium['height'] = Mage::getStoreConfig('vilpbrand/medium/height');
        $large['width'] = Mage::getStoreConfig('vilpbrand/large/width');
        $large['height'] = Mage::getStoreConfig('vilpbrand/large/height');

        return array('small' => $small, 'medium' => $medium, 'large' => $large);
    }

    public function deleteBrandImage($brand) {
		$title = strtolower($brand->getTitle());
        $subFolder = strtolower((string) $brand->getId() . $title);
        $length = strlen($subFolder);

        $path = Mage::getBaseDir('media') . DS . 'brand' . DS;
        if ($length == 1) {
            $firstFolder = $subFolder[0];
        } else {
            $firstFolder = $subFolder[0] . DS . $subFolder[1];
        }
        foreach ($this->getbrandConfig() as $size) {
            if (file_exists($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.jpg') ||
                    file_exists($path . $firstFolder . DS . $brand->getId().$title . '.jpg')) {
                unlink($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.jpg');
                unlink($path . $firstFolder . DS . $brand->getId().$title . '.jpg');
            } else if (file_exists($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.png') ||
                    file_exists($path . $firstFolder . DS . $brand->getId().$title . '.png')) {
                unlink($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.png');
                unlink($path . $firstFolder . DS . $brand->getId().$title . '.png');
            } else if (file_exists($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.jpeg') ||
                    file_exists($path . $firstFolder . DS . $brand->getId().$title . '.jpeg')) {
                unlink($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.jpeg');
                unlink($path . $firstFolder . DS . $brand->getId().$title . '.jpeg');
            } else if (file_exists($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.bmp') || 
                    file_exists($path . $firstFolder . DS . $brand->getId().$title . '.bmp')) {
                unlink($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.bmp');
                unlink($path . $firstFolder . DS . $brand->getId().$title . '.bmp');
            } else if(file_exists($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.gif') || 
                    file_exists($path . $firstFolder . DS . $brand->getId().$title . '.gif')){
                unlink($path . $firstFolder . DS . $brand->getId().$title . '_' . $size['width'] . '_' . $size['height'] . '.gif');
                unlink($path . $firstFolder . DS . $brand->getId().$title . '.gif');
            }
        }
    }

    public function getBrandImage($brand, $size = "small") {
        $config = $this->getbrandConfig();
		$title = strtolower($brand->getTitle());
        // there are 3sizes: small, medium, large
        $size = $config[$size];
        $w = $size['width'];
        $h = $size['height'];
        $subFolder = (string) $brand->getId() . $title;
        $length = strlen($subFolder);

        $path = Mage::getBaseDir('media') . DS . 'brand' . DS;
        if ($length == 1) {
            $firstFolder = $subFolder[0];
        } else {
            $firstFolder = $subFolder[0] . DS . $subFolder[1];
        }
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)
                . 'brand' . DS . $firstFolder . DS . $brand->getId().$title;
        if (file_exists($path . $firstFolder . DS . $brand->getId() .$title. '.jpg')) {
            $original = $path . $firstFolder . DS . $brand->getId() .$title. '.jpg';
            $cropImage = $path . $firstFolder . DS . $brand->getId() .$title. '_' . $w . '_' . $h . '.jpg';
            $image = $url . '_' . $w . '_' . $h . '.jpg';
        } else if (file_exists($path . $firstFolder . DS . $brand->getId() .$title. '.png')) {
            $original = $path . $firstFolder . DS . $brand->getId() .$title. '.png';
            $cropImage = $path . $firstFolder . DS . $brand->getId() .$title. '_' . $w . '_' . $h . '.png';
            $image = $url . '_' . $w . '_' . $h . '.png';
        } else if (file_exists($path . $firstFolder . DS . $brand->getId() .$title. '.jpeg')) {
            $original = $path . $firstFolder . DS . $brand->getId() .$title. '.jpeg';
            $cropImage = $path . $firstFolder . DS . $brand->getId() .$title. '_' . $w . '_' . $h . '.jpeg';
            $image = $url . '_' . $w . '_' . $h . '.jpeg';
        } else if (file_exists($path . $firstFolder . DS . $brand->getId() .$title. '.bmp')) {
            $original = $path . $firstFolder . DS . $brand->getId() .$title. '.bmp';
            $cropImage = $path . $firstFolder . DS . $brand->getId() .$title. '_' . $w . '_' . $h . '.bmp';
            $image = $url . '_' . $w . '_' . $h . '.bmp';
        } else {
            $original = $path . $firstFolder . DS . $brand->getId() .$title. '.gif';
            $cropImage = $path . $firstFolder . DS . $brand->getId() .$title. '_' . $w . '_' . $h . '.gif';
            $image = $url . '_' . $w . '_' . $h . '.gif';
        }

        if (@file_exists($cropImage)) {
            return $image;
        } else {
            $resizeObj = Mage::getModel('brand/resizeimage');
            $resizeObj->setFile($original);
            // *** 1) Initialise / load image
            // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
            $resizeObj->resizeImage($w, $h, 'crop');
            // *** 3) Save image
            $resizeObj->saveImage($cropImage, 100);
            return $image;
        }
    }

}