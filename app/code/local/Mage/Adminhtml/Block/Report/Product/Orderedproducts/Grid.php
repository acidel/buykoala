<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Report Sold Products Grid Block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */

class Mage_Adminhtml_Block_Report_Product_Orderedproducts_Grid extends Mage_Adminhtml_Block_Report_Grid
{
    /**
     * Sub report size
     *
     * @var int
     */
    protected $_subReportSize = 0;

    /**
     * Initialize Grid settings
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('gridProductsOrderedproducts');
    }

    /**
     * Prepare collection object for grid
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareCollection()
    {
		
        parent::_prepareCollection();
        $this->getCollection()
            ->initReport('reports/product_orderedproducts_collection');
		//echo"<pre>";
		//print_r($this->getCollection());


        return $this;
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareColumns()
    {
		 $this->addColumn('increment_id', array(
            'header'    =>Mage::helper('reports')->__('Order Number'),
            'index'     =>'increment_id',
			 'width' => '70px',
			 'type'      =>'text'
        ));
		  if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('reports')->__('Purchased from <br>(Store/website)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,  
				 'width' => '70px',
                'display_deleted' => true,
            ));
		  }
 
		$this->addColumn('distributor', array(
            'header'    =>Mage::helper('reports')->__('Distributor Name'),
			 'width' => '70px',
            'index'     =>'distributor'

        ));


        $this->addColumn('name', array(
            'header'    =>Mage::helper('reports')->__('Product Name'),
            'index'     =>'order_items_name',
			 'width' => '70px',
        ));

	

		$this->addColumn('psku', array(
            'header'    =>Mage::helper('reports')->__('SKU'),
            'index'     =>'psku',
			'width' => '70px',
			
        ));

			$this->addColumn('price', array(
            'header'    =>Mage::helper('reports')->__('Price'),
            'index'     =>'price',
				 'width' => '70px',
        ));

	$this->addColumn('base_currency_code', array(
            'header'    =>Mage::helper('reports')->__('Currency code'),
            'index'     =>'base_currency_code',
				 'width' => '70px',
        ));

			

		$this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reports')->__('Quantity Ordered'),
           
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
			'width' => '60px',
            'type'      =>'number'
        ));

		$this->addColumn('product_options', array(
            'header' => Mage::helper('reports')->__('Product Options'),
            'index' =>'product_options',
			'type'  => 'text',
			 'width' => '70px',
			'renderer' => 'Mage_Adminhtml_Block_Report_Product_Orderedproducts_Renderer_Options'
        ));
		


		$this->addColumn('firstname', array(
            'header'    =>Mage::helper('reports')->__('Customer First Name'),
            'index'     =>'firstname',
			 'width' => '70px',
        ));

		$this->addColumn('lastname', array(
            'header'    =>Mage::helper('reports')->__('Last Name'),
            'index'     =>'lastname',
			 'width' => '70px',
        ));

			$this->addColumn('email', array(
            'header'    =>Mage::helper('reports')->__('Customer Email'),
            'index'     =>'email',
				 'width' => '70px',
        ));

		$this->addColumn('street', array(
            'header'    =>Mage::helper('reports')->__('Street'),
            'index'     =>'street',
			 'width' => '70px',
        ));

		$this->addColumn('city', array(
            'header'    =>Mage::helper('reports')->__('City'),
            'index'     =>'city',
			 'width' => '70px',
        ));

			
		$this->addColumn('region', array(
            'header'    =>Mage::helper('reports')->__('Region'),
            'index'     =>'region',
			 'width' => '70px',
        ));
			$this->addColumn('postcode', array(
            'header'    =>Mage::helper('reports')->__('Postcode'),
            'index'     =>'postcode',
				 'width' => '70px',
        ));
	
		$this->addColumn('telephone', array(
            'header'    =>Mage::helper('reports')->__('Telephone'),
            'index'     =>'telephone',
			 'width' => '70px',
        ));

     

        $this->addExportType('*/*/exportOrderedProductsCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportOrderedProductsExcel', Mage::helper('reports')->__('Excel XML'));

        return parent::_prepareColumns();
    }
}
