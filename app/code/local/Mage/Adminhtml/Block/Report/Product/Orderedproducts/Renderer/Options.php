<?php
 class Mage_Adminhtml_Block_Report_Product_Orderedproducts_Renderer_Options extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
 {
	public function render(Varien_Object $row)
	{
	$value =  $row->getData($this->getColumn()->getIndex());
	$productOptions= unserialize($value);
	//echo"<pre>";
	//print_r($productOptions);	
	$ordered_options="";
    if (isset($productOptions['options'])) {
      foreach ($productOptions['options'] as $productOption) {
        $ordered_options .= $productOption['label']." : ".$productOption['value'].","; 
         }  
	  }		
	return $ordered_options=rtrim($ordered_options,","); 
	}
}