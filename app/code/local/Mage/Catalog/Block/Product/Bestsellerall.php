<?php 
	
Class Mage_Catalog_Block_Product_Bestsellerall extends Mage_Catalog_Block_Product_Abstract {


/**************************************************************************
Override _preparyLayout() method found in Mage_Core_Block_Abstract
Retrieve sold products collection, prepare toolbar

@Return Mage_Catalog_Block_Product_Bestseller
*************************************************************************/
public function _prepareLayout() {
	
	/*
$storeId = Mage::app()->getStore()->getId();
$products = Mage::getResourceModel('reports/product_collection')
->addOrderedQty()
->addAttributeToSelect('*') //Need this so products show up correctly in product listing
->setStoreId($storeId)
->addStoreFilter($storeId);
Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
$this->setToolbar($this->getLayout()->createBlock('catalog/product_list_toolbar', 'Toolbar'));
$toolbar = $this->getToolbar();
$toolbar->setAvailableOrders(array(
'ordered_qty' => $this->__('Most Purchased'),
'name' => $this->__('Name'),
'price' => $this->__('Price')
))
->setDefaultOrder('ordered_qty')
->setDefaultDirection('desc')
->setCollection($products);
return $this;
*/
 $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToFilter('news_from_date', array('or'=> array(
                0 => array('date' => true, 'to' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter('news_to_date', array('or'=> array(
                0 => array('date' => true, 'from' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter(
                array(
                    array('attribute' => 'news_from_date', 'is'=>new Zend_Db_Expr('not null')),
                    array('attribute' => 'news_to_date', 'is'=>new Zend_Db_Expr('not null'))
                    )
              )
            ->addAttributeToSort('news_from_date', 'desc')
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1)
        ;
		$this->setProductCollection($collection);

return $this;


}




/**************************************************************************
Retrieve product collection. A protected function in keeping
with OOP principals

@Return Mage_Reports_Model_Mysql4_Product_Collection
*************************************************************************/
protected function _getProductCollection() {
	
return $this->getToolbar()->getCollection();
}

/**************************************************************************
Public interface to read toobar object template HTML

@Return String (HTML for Toolbar)
*************************************************************************/
public function getToolbarHtml() {
return $this->getToolbar()->_toHtml();
}

/**************************************************************************
Public interface to get list vs grid mode form toolbar object.

@Return String (grid || list)
*************************************************************************/
public function getMode() {
return $this->getToolbar()->getCurrentMode();
}

/**************************************************************************
Public interface and alias to protected _getProductCollection method

@Return Mage_Reports_Model_Mysql4_Product_Collection
*************************************************************************/
public function getLoadedProductCollection() {
return $this->_getProductCollection();
}
}