<?php
class Mage_Catalog_Block_Product_Onsale extends Mage_Catalog_Block_Product_List
{

protected $categories;

  function __construct() {    
	$category = Mage::getModel('catalog/category')->load(55); //category id 55 for buykoala at local it is 56
	$this->categories=$category->getAllChildren(true);
   }

   function get_prod_count()
   {
      //unset any saved limits
      Mage::getSingleton('catalog/session')->unsLimitPage();
      return (isset($_REQUEST['limit'])) ? intval($_REQUEST['limit']) : 9;
   }// get_prod_count

   function get_cur_page()
   {
      return (isset($_REQUEST['p'])) ? intval($_REQUEST['p']) : 1;
   }// get_cur_page

   /**
    * Retrieve loaded category collection
    *
    * @return Mage_Eav_Model_Entity_Collection_Abstract
   **/
   protected function _getProductCollection()
   {
		
      $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
      $collection = Mage::getResourceModel('catalog/product_collection');
      $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
      $collection = $this->_addProductAttributesAndPrices($collection)
         ->addStoreFilter()		 
		 ->addAttributeToFilter('attribute_set_id',array('4'))  
         ->addAttributeToFilter('special_price',array('notnull' => true))   
			 	->joinField('category_id',
					 'catalog/category_product',
					  'category_id',
					   'product_id=entity_id',
					   null,
				  'inner')		   
			->addAttributeToFilter('category_id', array('in' => $this->categories))
       //  ->setPageSize($this->get_prod_count())
         ->setCurPage($this->get_cur_page());
		$collection->getSelect()->group('e.entity_id');	
      $this->setProductCollection($collection);
      return $collection;
   }// _getProductCollection
}// Mage_Catalog_Block_Product_New
?>