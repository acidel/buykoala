<?php
 
class Mycode_Function_Helper_Data extends Mage_Core_Helper_Abstract
{
	
 	public function weekly_detailed_report_v2(){

		$day = new DateTime();
		$day->modify( "-7 day" );
	 
		$fromDate = $day->format("Y-m-d 00:00:00");
		
		$weekday = "Saturday";
		if (date('l') == "$weekday")
		{
			$week = strtotime("last $weekday");
		}
		else 
		{
			$week = strtotime("last $weekday-1 week");
		}
		
		$startdate = (date("Y-m-d 00:00:00", $week));
		
		$enddate = date("Y-m-d 00:00:00", $week + 7*86400);
		
		// $sat= date("Y-m-d 00:00:00", mktime(0,0,0,9,22,2012));
	 //$start = date("Y-m-d 00:00:00", mktime(0,0,0,9,15,2012));
	 	$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at<'$enddate' and created_at>'$startdate'";
	 	//echo $sql;die();
	//	$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at>'$fromDate'";
		$orders = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
	
		
		
		foreach ($orders as $neworder)
		{
			
			$order_id = $neworder['order_id'];
			
			$order = Mage::getModel('sales/order')->load($order_id);
			$date = $order->getCreatedAt();
			$_shippingAddress = $order->getBillingAddress();
    		
    		$date = "".date("d/m/Y",strtotime($date))."";

			$fname = $order['customer_firstname'];
			$lname = $order['customer_lastname'];
			
			$purchase_date  =Mage::app()->getLocale()->date(strtotime($order['updated_at']), null, null, false)->toString('dd/MM/yyyy');
			$city = $_shippingAddress->getCity();
			$address = $_shippingAddress->getStreetFull();
			$state = $_shippingAddress->getRegion() ;
			$postcode = $_shippingAddress->getPostcode();
			$phone = $_shippingAddress->getTelephone();
			$country_code = $_shippingAddress->getCountry();
			
			$country=Mage::app()->getLocale()->getCountryTranslation($country_code);
			
			$items = $order->getAllItems();
			
			$itemcount=count($items);
			//echo $itemcount;die();
			$name=array();
			$unitPrice=array();
			$sku=array();
			$ids=array();
			$qty=array();
			foreach ($items as $itemId => $item)
			{
				
				$ids=$item->getProductId();
				//echo $item->getStatus();
				$sql = "SELECT merchant_id FROM groupdeals WHERE product_id = $ids";
				$res = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sql);
				if ($res && $res!="")
				{
					$item_id = $item->getId();
					
					$_prod = Mage::getModel('catalog/product')->load($ids);
					
					if ($_prod->getData('commission_percent') > 0)
					{
						$commis_percent = $_prod->getData('commission_percent');//commission_percent
					}
					else
					{
						$commis_percent = 0;
					}
					
					if ($_prod->getData('commission_fixed') > 0)
					{
						$commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
					}
					else
					{
						$commis_fixed = 0;
					}
					
					if ($_prod->getData('aammount') > 0)
					{
						$additional_ammount = $_prod->getData('aammount');//aammount
					}
					else
					{
						$additional_ammount = 0;
					}
				
				
				//echo "($ids, $res)";
				
					$sql = "SELECT twitter FROM groupdeals_merchants WHERE merchants_id = $res";
					$freq = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sql);
					// echo "$freq </br>"; 
					$sql = "SELECT product_options FROM sales_flat_order_item WHERE product_id = $ids AND order_id = $order_id and item_id = $item_id";
					$opts = unserialize(Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql));
					
					$sql = "SELECT coupon_code FROM groupdeals_coupons WHERE order_item_id = $item_id";
					$code = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
					unset($coupon);
					foreach ($code as $value)
					{
						$coupon.="".$value["coupon_code"]."-";
					}
					$coupon = substr($coupon,0,-1);
					
					unset ($variable);
						foreach ($opts['options'] as $option)
						{
							
							$variable[] = "".$option['label'].": ".$option['value']."";
						
						}
					
					
				    $prodname = $item->getName();
				   	$commission = (100 - $commis_percent) / 100;
				    if ($commission != 1)
				    {
				    	$unitPrice = ($item->getBasePriceInclTax()- $additional_ammount) * $commission;
				    }
				    else
				    {
				    	$unitPrice = ($item->getBasePriceInclTax() - $additional_ammount)  - $commis_fixed;
				    }
				    
				    $sku=$item->getSku();
				    
				    $qty=$item->getQtyOrdered();
				    //echo $freq;die();
				  
				    if ($res > 0 && $freq == '0||1')//weekely report
				    {
				    	
				    	
				    	$searchArray = array("\r\n", ",", "\n");
						$replaceArray = array(" ", "-", " ");
	
				    	
				    	$products_row[$ids][] = array($order->getIncrementId(), $date, $coupon, $fname, $lname, str_replace($searchArray, $replaceArray, $address),
				    	$city, $state, $postcode, $country, $phone, $prodname, $ids, "$".($item->getBasePriceInclTax()-$additional_ammount)."", $qty, $variable[0], $variable[1]);
				    	
						$report_summery[$ids]['qty']+= $qty;
				    	$report_summery[$ids]['revenue']+= ($qty * ($item->getBasePriceInclTax()-$additional_ammount));
				    	$report_summery[$ids]['merchant']+= ($qty * $unitPrice);
						$merchant[$ids] = $res;
						$product_name[$ids] = $prodname;
				    	
				    }
				}
			    
			}
			
			
		}	
		//echo "blah";die();
		$header = array('Order ID','Transaction Date', 'Coupon Code','First Name','Last Name','Address','City','State','Postcode','Country','Phone Number','Product Name', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Option 1','Option 2');
		
		//echo "fdsjkhg";die();
		//echo "<pre>"; print_r($products_row);die();
		foreach ($products_row as $key=>$value)
		{
			unset ($data);
			//echo $key;die();
			$data[] = $header;
			foreach ($value as $newvalue)
			{
				$data[] = $newvalue;
			}
			$data[]=array("","");
			$data[]=array("","");
			$data[]=array("Total Items",$report_summery[$key]['qty']);
			$data[]=array("Total Revenue($)","$".$report_summery[$key]['revenue']."");
			$data[]=array("Total Commission($)","$".($report_summery[$key]['revenue'] - $report_summery[$key]['merchant'])."");
			$data[]=array("Total payable to merchant($)","$".$report_summery[$key]['merchant']."");
			
			$this->create_pdf($data);
			//echo "<pre>"; print_r($data);die();
			$mage_csv = new Varien_File_Csv();
			$file_path = '/tmp/sample.csv';
			//echo ($merchant[$key]);die();
			$sql = "SELECT email, name FROM groupdeals_merchants WHERE merchants_id = ".$merchant[$key]."";
			//echo $sql;die();
			$email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
			//echo "<pre>";print_r($email_add);die();
			$email_ser = explode('||', $email_add[0]['email']);
			$email = $email_ser[1];
			
			$name_ser = explode('||', $email_add[0]['name']);
			$name = $name_ser[1];
			
			//echo $name;echo $email;die();
			$mage_csv->saveData($file_path, $data); 
			
			try
			{
				$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
				$mail = new Zend_Mail();
				$mail->setFrom("deals@ikoala.com.au","iKoala Team");
				$mail->addTo($email,$name);//$email
				
				$mail->addCc('merchantsalesreports@ikoala.com.au','ikOala');//$email
				$mail->addCc('accounts@ikoala.com.au','ikOala');//$email
				$mail->setSubject("Weekly Reports");
				$mail->setBodyHtml("Dear $name, <br/><br/> Please find attached the weekly reports of your products. <br/><br/>Sincerly,<br/>iKoala Team"); // here u also use setBodyText options.
				 
				$fileContents = file_get_contents($file_path);
				$file = $mail->createAttachment($fileContents);
				$file->filename = "".$name."_sales_reports_".$product_name[$key]."_".$today.".csv";
	
				$fileContents = file_get_contents('/tmp/test1.pdf');
				$file = $mail->createAttachment($fileContents);
				$file->filename = "".$name."_sales_reports_".$product_name[$key]."_".$today.".pdf";
				
				$mail->send();
				//echo "done";die();
	
			}
			catch(Exception $e)
			{
				echo $e->getMassage();
			}
		
		}
		
	}
	
	/////////////////////////////////////////
	
 	public function daily_detailed_report_v2(){

		$day = new DateTime();
		$day->modify( "-1 day" );
	 
		$fromDate = $day->format("Y-m-d 00:00:00");
	
		$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at>'$fromDate'";
		$orders = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
	
		
		
		foreach ($orders as $neworder)
		{
			
			
			
			$order_id = $neworder['order_id'];
			$order = Mage::getModel('sales/order')->load($order_id);
			$date = $order->getCreatedAt();
			$_shippingAddress = $order->getBillingAddress();
    		
    		$date = "".date("d/m/Y",strtotime($date))."";
			
    		
    
    		//$address = Mage::getModel('sales/order_address')->load($order_id);

			//echo "pre"; print_r($_shippingAddress->getCity());die();
			$fname = $order['customer_firstname'];
			$lname = $order['customer_lastname'];
			
			$purchase_date  =Mage::app()->getLocale()->date(strtotime($order['updated_at']), null, null, false)->toString('dd/MM/yyyy');
			$city = $_shippingAddress->getCity();
			$address = $_shippingAddress->getStreetFull();
			$state = $_shippingAddress->getRegion() ;
			$postcode = $_shippingAddress->getPostcode();
			$phone = $_shippingAddress->getTelephone();
			$country_code = $_shippingAddress->getCountry();
			
			$country=Mage::app()->getLocale()->getCountryTranslation($country_code);
			//echo $address;die();
			//echo "<pre>";print_r($state);die();
			
			
			$items = $order->getAllItems();
			//$options = $order->getItemOptions();
			
		//	echo "<pre>";print_r($options);die();
			$itemcount=count($items);
			$name=array();
			$unitPrice=array();
			$sku=array();
			$ids=array();
			$qty=array();
			foreach ($items as $itemId => $item)
			{
				
				$ids=$item->getProductId();
				$item_id = $item->getId();
				
				$_prod = Mage::getModel('catalog/product')->load($ids);
				
				if ($_prod->getData('commission_percent') > 0)
				{
					$commis_percent = $_prod->getData('commission_percent');//commission_percent
				}
				else
				{
					$commis_percent = 0;
				}
				
				if ($_prod->getData('commission_fixed') > 0)
				{
					$commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
				}
				else
				{
					$commis_fixed = 0;
				}
				
				if ($_prod->getData('aammount') > 0)
				{
					$additional_ammount = $_prod->getData('aammount');//aammount
				}
				else
				{
					$additional_ammount = 0;
				}
				
				$sql = "SELECT merchant_id FROM groupdeals WHERE product_id = $ids";
				$res = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				$sql = "SELECT twitter FROM groupdeals_merchants WHERE merchants_id = $res";
				$freq = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				
				$sql = "SELECT product_options FROM sales_flat_order_item WHERE product_id = $ids AND order_id = $order_id and item_id = $item_id";
				$opts = unserialize(Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql));
				
				$sql = "SELECT coupon_code FROM groupdeals_coupons WHERE order_item_id = $item_id";
				$code = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
				unset($coupon);
foreach ($code as $value)
{
	$coupon.="".$value["coupon_code"]."-";
}
$coupon = substr($coupon,0,-1);

				unset ($variable);
					foreach ($opts['options'] as $option)
					{
						//echo "<pre>";print_r($option);die();
						$variable[] = "".$option['label'].": ".$option['value']."";
					
					}
				/*if ($ids == 73)
					{
						echo "<pre>";print_r($variable[1]);die();
					}*/
				
			    $prodname = $item->getName();
			   	$commission = (100 - $commis_percent) / 100;
			    if ($commission != 1)
			    {
			    	$unitPrice = ($item->getBasePriceInclTax() - $additional_ammount) * $commission;
			    }
			    else
			    {
			    	$unitPrice = ($item->getBasePriceInclTax() - $additional_ammount)  - $commis_fixed;
			    }
			    $sku=$item->getSku();
			    
			    $qty=$item->getQtyOrdered();
			    //echo $freq;die();
			  
			    if ($res > 0 && $freq == '0||2')//daily report
			    {
			    	//$products_row[$ids][] = array('Order ID', 'Coupon Code','First Name','Last Name','Address','City','State','Postcode','Phone Number','Product Name', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Option 1','Option 2');
			    	
			    	$searchArray = array("\r\n", ",", "\n");
					$replaceArray = array(" ", "-", " ");
			    	
			    	$products_row[$ids][] = array($order->getIncrementId(),$date, $coupon, $fname, $lname,  str_replace($searchArray, $replaceArray, $address),
			    	$city, $state, $postcode, $phone, $prodname, $ids, "$".($item->getBasePriceInclTax() - $additional_ammount)."", $qty, $variable[0], $variable[1]);
			    	
					$report_summery[$ids]['qty']+= $qty;
			    	$report_summery[$ids]['revenue']+= ($qty * ($item->getBasePriceInclTax() - $additional_ammount));
			    	$report_summery[$ids]['merchant']+= ($qty * $unitPrice);
					$merchant[$ids] = $res;
					$product_name[$ids] = $prodname;
			    	
			    }
			}
			
		}	
		
		$header = array('Order ID','Transaction Date', 'Coupon Code','First Name','Last Name','Address','City','State','Postcode','Phone Number','Product Name', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Option 1','Option 2');
		
		//echo "fdsjkhg";die();
		//echo "<pre>"; print_r($products_row);die();
		foreach ($products_row as $key=>$value)
		{
			unset ($data);
			//echo $key;die();
			$data[] = $header;
			foreach ($value as $newvalue)
			{
				$data[] = $newvalue;
			}
			$data[]=array("","");
			$data[]=array("","");
			$data[]=array("Total Items",$report_summery[$key]['qty']);
			$data[]=array("Total Revenue($)","$".$report_summery[$key]['revenue']."");
			$data[]=array("Total Commission($)","$".($report_summery[$key]['revenue'] - $report_summery[$key]['merchant'])."");
			$data[]=array("Total payable to merchant($)","$".$report_summery[$key]['merchant']."");
			
			//echo "<pre>"; print_r($data);die();
			$mage_csv = new Varien_File_Csv();
			$file_path = '/tmp/sample.csv';
			//echo ($merchant[$key]);die();
			$sql = "SELECT email, name FROM groupdeals_merchants WHERE merchants_id = ".$merchant[$key]."";
			//echo $sql;die();
			$email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
			//echo "<pre>";print_r($email_add);die();
			$email_ser = explode('||', $email_add[0]['email']);
			$email = $email_ser[1];
			
			$name_ser = explode('||', $email_add[0]['name']);
			$name = $name_ser[1];
			
			//echo $name;echo $email;die();
			$mage_csv->saveData($file_path, $data); 
			
			try
			{
				$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
				$mail = new Zend_Mail();
				$mail->setFrom("deals@ikoala.com.au","iKoala Team");
				$mail->addTo($email,$name);//$email
				
				$mail->addCc('merchantsalesreports@ikoala.com.au','ikOala');//$email
				$mail->addCc('accounts@ikoala.com.au','ikOala');//$email
				$mail->setSubject("Daily Reports");
				$mail->setBodyHtml("Dear $name, <br/><br/> Please find attached the daily reports of your products. <br/><br/>Sincerly,<br/>iKoala Team"); // here u also use setBodyText options.
				 
				$fileContents = file_get_contents($file_path);
				$file = $mail->createAttachment($fileContents);
				$file->filename = "".$name."_sales_reports_".$product_name[$key]."_".$today.".csv";
	
				$mail->send();
	
			}
			catch(Exception $e)
			{
				echo $e->getMassage();
			}
		
		}
		
	}
	
	/////////////////////////////////////////
	
	
 	public function weekly_detailed_report(){

		$day = new DateTime();
		$day->modify( "-7 day" );
	 
		$fromDate = $day->format("Y-m-d 00:00:00");
	
		$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at>'$fromDate'";
		$orders = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
	
		foreach ($orders as $neworder)
		{
			
			
			
			$order_id = $neworder['order_id'];
			$order = Mage::getModel('sales/order')->load($order_id);
			
			$_shippingAddress = $order->getBillingAddress();
    		
    
    		//$address = Mage::getModel('sales/order_address')->load($order_id);

			//echo "pre"; print_r($_shippingAddress->getCity());die();
			$fname = $order['customer_firstname'];
			$lname = $order['customer_lastname'];
			
			$purchase_date  =Mage::app()->getLocale()->date(strtotime($order['updated_at']), null, null, false)->toString('dd/MM/yyyy');
			$city = $_shippingAddress->getCity();
			$address = $_shippingAddress->getStreetFull();
			$state = $_shippingAddress->getRegion() ;
			$postcode = $_shippingAddress->getPostcode();
			$phone = $_shippingAddress->getTelephone();
			//echo $address;die();
			//echo "<pre>";print_r($state);die();
			
			
			$items = $order->getAllItems();
			//$options = $order->getItemOptions();
			
		//	echo "<pre>";print_r($options);die();
			$itemcount=count($items);
			$name=array();
			$unitPrice=array();
			$sku=array();
			$ids=array();
			$qty=array();
			foreach ($items as $itemId => $item)
			{
				
				$ids=$item->getProductId();
				$item_id = $item->getId();
				
				$_prod = Mage::getModel('catalog/product')->load($ids);
				
				if ($_prod->getData('commission_percent') > 0)
				{
					$commis_percent = $_prod->getData('commission_percent');//commission_percent
				}
				else
				{
					$commis_percent = 0;
				}
				
				if ($_prod->getData('commission_fixed') > 0)
				{
					$commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
				}
				else
				{
					$commis_fixed = 0;
				}
				
				if ($_prod->getData('aammount') > 0)
				{
					$additional_ammount = $_prod->getData('aammount');//aammount
				}
				else
				{
					$additional_ammount = 0;
				}
				
				$sql = "SELECT merchant_id FROM groupdeals WHERE product_id = $ids";
				$res = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				$sql = "SELECT twitter FROM groupdeals_merchants WHERE merchants_id = $res";
				$freq = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				
				$sql = "SELECT product_options FROM sales_flat_order_item WHERE product_id = $ids AND order_id = $order_id and item_id = $item_id";
				$opts = unserialize(Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql));
				
				$sql = "SELECT coupon_code FROM groupdeals_coupons WHERE order_item_id = $item_id";
				$coupon = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);

				unset ($variable);
					foreach ($opts['options'] as $option)
					{
						//echo "<pre>";print_r($option);die();
						$variable[] = "".$option['label'].": ".$option['value']."";
					
					}
				/*if ($ids == 73)
					{
						echo "<pre>";print_r($variable[1]);die();
					}*/
				
			    $name = $item->getName();
			   	$commission = (100 - $commis_percent) / 100;
			    if ($commission != 1)
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount) * $commission;
			    }
			    else
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount)  - $commis_fixed;
			    }
			    $sku=$item->getSku();
			    
			    $qty=$item->getQtyOrdered();
			    
			    if ($res > 0 && $freq == '0||1')//weekely report
			    {
			    	
			    	$product_order_details[$res][$item_id]['order'] = $order->getIncrementId();
			    	$product_order_details[$res][$item_id]['coupon'] = $coupon;
			    	$product_order_details[$res][$item_id]['fname'] = $fname;
			    	$product_order_details[$res][$item_id]['lname'] = $lname;
			    	$product_order_details[$res][$item_id]['address'] = str_replace(",","-",$address);
			    	$product_order_details[$res][$item_id]['city'] = $city;
			    	$product_order_details[$res][$item_id]['state'] = $state;
			    	$product_order_details[$res][$item_id]['pcode'] = $postcode;
			    	$product_order_details[$res][$item_id]['phone'] = $phone;
			    	
			    	$product_order_details[$res][$item_id]['option1'] = $variable[0];
			    	$product_order_details[$res][$item_id]['option2'] = $variable[1];
			    	

			    	$product_order_details[$res][$item_id]['name'] = $name;
			    	$product_order_details[$res][$item_id]['ids'] = $ids;
			    	$product_order_details[$res][$item_id]['unitPrice'] = $item->getPrice();
			    	$product_order_details[$res][$item_id]['qty'] += $qty;
			    	$product_order_details[$res][$item_id]['total_merchant'] += ($qty * $unitPrice);
			    	$product_order_details[$res][$item_id]['total_rev'] += ($qty * $item->getPrice());
			    	
			    	$product_order_details[$res][$item_id]['aa'] = ($qty * $additional_ammount);
			    	$product_order_details[$res][$item_id]['com'] = ($commis_percent);
			    	$product_order_details[$res][$item_id]['com_fixed'] = ($qty * $commis_fixed);
			    	
			    	
			    	$report_summery[$res]['qty']+= $qty;
			    	$report_summery[$res]['revenue']+= ($qty * $item->getPrice());
			    	$report_summery[$res]['merchant']+= ($qty * $unitPrice);
			    	
			    	
			    	
			    }
			}
			
		}
	  	$mage_csv = new Varien_File_Csv();
	 	$file_path = '/tmp/sample.csv';
		foreach($product_order_details as $merch_id=>$arr)
		{
			unset($products_row);
			$products_row[] = array('Order ID', 'Coupon Code','First Name','Last Name','Address','City','State','Postcode','Phone Number','Product Name', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Option 1','Option 2', 'Total Revenue($)', 'Additional Ammount ($)', 'Commission Rate(%)','Commission Rate(fixed)', 'Net Payable to Merchant($)', 'Net Revenue iKoala($)');
			foreach ($arr as $key=>$value)
			{
				
			    $data = array();
			    
			    $data[] = $value['order'];
			    $data[] = $value['coupon'];
			    $data[] = $value['fname'];
			    $data[] = $value['lname'];
			    $data[] = $value['address'];
			    $data[] = $value['city'];
			    $data[] = $value['state'];
			    $data[] = $value['pcode'];
			    $data[] = $value['phone'];
			    
			    
			    $data[] = $value['name'];
			    $data[] = $value['ids'];
			    $data[] = $value['unitPrice'];
			    $data[] = $value['qty'];
			    $data[] = $value['option1'];
			    $data[] = $value['option2'];
			    
			    $data[] = $value['total_rev'];
			    
			    
			    $data[] = $value['aa'];
			    $data[] = $value['com'];
			    $data[] = $value['com_fixed'];	
			    $data[] = $value['total_merchant'];
			    $data[] = $value['total_rev'] - $value['total_merchant'];
			    	
			    $products_row[] = $data;
			    
			}
			$products_row[]=array("","");
			$products_row[]=array("","");
			$products_row[]=array("Total Items",$report_summery[$merch_id]['qty']);
			$products_row[]=array("Total Revenue($)",$report_summery[$merch_id]['revenue']);
			$products_row[]=array("Total Commission($)",($report_summery[$merch_id]['revenue'] - $report_summery[$merch_id]['merchant']));
			$products_row[]=array("Total payable to merchant($)",$report_summery[$merch_id]['merchant']);
			
			
			
			$sql = "SELECT email, name FROM groupdeals_merchants WHERE merchants_id = $merch_id";
			$email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
			//echo "<pre>";print_r($email_add);die();
			$email_ser = explode('||', $email_add[0]['email']);
			$email = $email_ser[1];
			
			$name_ser = explode('||', $email_add[0]['name']);
			$name = $name_ser[1];
			
			//echo $name;echo $email;die();
			$mage_csv->saveData($file_path, $products_row); 
			
			try
			{
				$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
				$mail = new Zend_Mail();
				$mail->setFrom("deals@ikoala.com.au","iKoala Team");
				$mail->addTo($email,$name);//$email
				$mail->addCc('merchantsalesreports@ikoala.com.au','ikOala');//$email
				$mail->addCc('accounts@ikoala.com.au','ikOala');//$email
				$mail->setSubject("Daily Reports");
				$mail->setBodyHtml("Dear $name, <br/><br/> Please find attached the daily reports of your products. <br/><br/>Sincerly,<br/>iKoala Team"); // here u also use setBodyText options.
				 
				$fileContents = file_get_contents($file_path);
				$file = $mail->createAttachment($fileContents);
				$file->filename = "daily_reports_$today.csv";

				$mail->send();
 
			}
			catch(Exception $e)
			{
				echo $e->getMassage();
			}
			
		}
	}
	
	
	 public function daily_detailed_report(){

		$day = new DateTime();
		$day->modify( "-1 day" );
	 
		$fromDate = $day->format("Y-m-d 00:00:00");
	
		$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at>'$fromDate'";
		$orders = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
	
		foreach ($orders as $neworder)
		{
			
			
			
			$order_id = $neworder['order_id'];
			$order = Mage::getModel('sales/order')->load($order_id);
			
			$_shippingAddress = $order->getBillingAddress();
    		
    
    		//$address = Mage::getModel('sales/order_address')->load($order_id);

			//echo "pre"; print_r($_shippingAddress->getCity());die();
			$fname = $order['customer_firstname'];
			$lname = $order['customer_lastname'];
			
			$purchase_date  =Mage::app()->getLocale()->date(strtotime($order['updated_at']), null, null, false)->toString('dd/MM/yyyy');
			$city = $_shippingAddress->getCity();
			$address = $_shippingAddress->getStreetFull();
			$state = $_shippingAddress->getRegion() ;
			$postcode = $_shippingAddress->getPostcode();
			$phone = $_shippingAddress->getTelephone();
			//echo $address;die();
			//echo "<pre>";print_r($state);die();
			
			
			$items = $order->getAllItems();
			//$options = $order->getItemOptions();
			
		//	echo "<pre>";print_r($options);die();
			$itemcount=count($items);
			$name=array();
			$unitPrice=array();
			$sku=array();
			$ids=array();
			$qty=array();
			foreach ($items as $itemId => $item)
			{
				
				$ids=$item->getProductId();
				$item_id = $item->getId();
				
				$_prod = Mage::getModel('catalog/product')->load($ids);
				
				if ($_prod->getData('commission_percent') > 0)
				{
					$commis_percent = $_prod->getData('commission_percent');//commission_percent
				}
				else
				{
					$commis_percent = 0;
				}
				
				if ($_prod->getData('commission_fixed') > 0)
				{
					$commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
				}
				else
				{
					$commis_fixed = 0;
				}
				
				if ($_prod->getData('aammount') > 0)
				{
					$additional_ammount = $_prod->getData('aammount');//aammount
				}
				else
				{
					$additional_ammount = 0;
				}
				
				$sql = "SELECT merchant_id FROM groupdeals WHERE product_id = $ids";
				$res = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				$sql = "SELECT twitter FROM groupdeals_merchants WHERE merchants_id = $res";
				$freq = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				
				$sql = "SELECT product_options FROM sales_flat_order_item WHERE product_id = $ids AND order_id = $order_id and item_id = $item_id";
				$opts = unserialize(Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql));
				
				$sql = "SELECT coupon_code FROM groupdeals_coupons WHERE order_item_id = $item_id";
				$coupon = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);

				unset ($variable);
					foreach ($opts['options'] as $option)
					{
						//echo "<pre>";print_r($option);die();
						$variable[] = "".$option['label'].": ".$option['value']."";
					
					}
				/*if ($ids == 73)
					{
						echo "<pre>";print_r($variable[1]);die();
					}*/
				
			    $name = $item->getName();
			   	$commission = (100 - $commis_percent) / 100;
			    if ($commission != 1)
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount) * $commission;
			    }
			    else
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount)  - $commis_fixed;
			    }
			    $sku=$item->getSku();
			    
			    $qty=$item->getQtyOrdered();
			    
			    if ($res > 0 && $freq == '0||2')//daily report
			    {
			    	
			    	$product_order_details[$res][$item_id]['order'] = $order->getIncrementId();
			    	$product_order_details[$res][$item_id]['coupon'] = $coupon;
			    	$product_order_details[$res][$item_id]['fname'] = $fname;
			    	$product_order_details[$res][$item_id]['lname'] = $lname;
			    	$product_order_details[$res][$item_id]['address'] = str_replace(",","-",$address);
			    	$product_order_details[$res][$item_id]['city'] = $city;
			    	$product_order_details[$res][$item_id]['state'] = $state;
			    	$product_order_details[$res][$item_id]['pcode'] = $postcode;
			    	$product_order_details[$res][$item_id]['phone'] = $phone;
			    	
			    	$product_order_details[$res][$item_id]['option1'] = $variable[0];
			    	$product_order_details[$res][$item_id]['option2'] = $variable[1];
			    	

			    	$product_order_details[$res][$item_id]['name'] = $name;
			    	$product_order_details[$res][$item_id]['ids'] = $ids;
			    	$product_order_details[$res][$item_id]['unitPrice'] = $item->getPrice();
			    	$product_order_details[$res][$item_id]['qty'] += $qty;
			    	$product_order_details[$res][$item_id]['total_merchant'] += ($qty * $unitPrice);
			    	$product_order_details[$res][$item_id]['total_rev'] += ($qty * $item->getPrice());
			    	
			    	$product_order_details[$res][$item_id]['aa'] = ($qty * $additional_ammount);
			    	$product_order_details[$res][$item_id]['com'] = ($commis_percent);
			    	$product_order_details[$res][$item_id]['com_fixed'] = ($qty * $commis_fixed);
			    	
			    	
			    	$report_summery[$res]['qty']+= $qty;
			    	$report_summery[$res]['revenue']+= ($qty * $item->getPrice());
			    	$report_summery[$res]['merchant']+= ($qty * $unitPrice);
			    	
			    	
			    	
			    }
			}
			
		}
	  	$mage_csv = new Varien_File_Csv();
	 	$file_path = '/tmp/sample.csv';
		foreach($product_order_details as $merch_id=>$arr)
		{
			unset($products_row);
			$products_row[] = array('Order ID', 'Coupon Code','First Name','Last Name','Address','City','State','Postcode','Phone Number','Product Name', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Option 1','Option 2', 'Total Revenue($)', 'Additional Ammount ($)', 'Commission Rate(%)','Commission Rate(fixed)', 'Net Payable to Merchant($)', 'Net Revenue iKoala($)');
			foreach ($arr as $key=>$value)
			{
				
			    $data = array();
			    
			    $data[] = $value['order'];
			    $data[] = $value['coupon'];
			    $data[] = $value['fname'];
			    $data[] = $value['lname'];
			    $data[] = $value['address'];
			    $data[] = $value['city'];
			    $data[] = $value['state'];
			    $data[] = $value['pcode'];
			    $data[] = $value['phone'];
			    
			    
			    $data[] = $value['name'];
			    $data[] = $value['ids'];
			    $data[] = $value['unitPrice'];
			    $data[] = $value['qty'];
			    $data[] = $value['option1'];
			    $data[] = $value['option2'];
			    
			    $data[] = $value['total_rev'];
			    
			    
			    $data[] = $value['aa'];
			    $data[] = $value['com'];
			    $data[] = $value['com_fixed'];	
			    $data[] = $value['total_merchant'];
			    $data[] = $value['total_rev'] - $value['total_merchant'];
			    	
			    $products_row[] = $data;
			    
			}
			$products_row[]=array("","");
			$products_row[]=array("","");
			$products_row[]=array("Total Items",$report_summery[$merch_id]['qty']);
			$products_row[]=array("Total Revenue($)",$report_summery[$merch_id]['revenue']);
			$products_row[]=array("Total Commission($)",($report_summery[$merch_id]['revenue'] - $report_summery[$merch_id]['merchant']));
			$products_row[]=array("Total payable to merchant($)",$report_summery[$merch_id]['merchant']);
			
			
			
			$sql = "SELECT email, name FROM groupdeals_merchants WHERE merchants_id = $merch_id";
			$email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
			//echo "<pre>";print_r($email_add);die();
			$email_ser = explode('||', $email_add[0]['email']);
			$email = $email_ser[1];
			
			$name_ser = explode('||', $email_add[0]['name']);
			$name = $name_ser[1];
			
			//echo $name;echo $email;die();
			$mage_csv->saveData($file_path, $products_row); 
			
			try
			{
				$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
				$mail = new Zend_Mail();
				$mail->setFrom("deals@ikoala.com.au","iKoala Team");
				$mail->addTo($email,$name);//$email
				$mail->addCc('merchantsalesreports@ikoala.com.au','ikOala');//$email
				$mail->addCc('accounts@ikoala.com.au','ikOala');//$email
				$mail->setSubject("Daily Reports");
				$mail->setBodyHtml("Dear $name, <br/><br/> Please find attached the daily reports of your products. <br/><br/>Sincerly,<br/>iKoala Team"); // here u also use setBodyText options.
				 
				$fileContents = file_get_contents($file_path);
				$file = $mail->createAttachment($fileContents);
				$file->filename = "daily_reports_$today.csv";

				$mail->send();
 
			}
			catch(Exception $e)
			{
				echo $e->getMassage();
			}
			
		}
	}
	
	
	public function send_daily_report(){

		$day = new DateTime();
		$day->modify( "-1 day" );
	 
		$fromDate = $day->format("Y-m-d 00:00:00");
	
		$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at>'$fromDate'";
		$orders = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
	
		foreach ($orders as $neworder)
		{
			
			$order_id = $neworder['order_id'];
			$order = Mage::getModel('sales/order')->load($order_id);
			$items = $order->getAllItems();
			
			$itemcount=count($items);
			$name=array();
			$unitPrice=array();
			$sku=array();
			$ids=array();
			$qty=array();
			foreach ($items as $itemId => $item)
			{
				
				$ids=$item->getProductId();
				
				$_prod = Mage::getModel('catalog/product')->load($ids);
				
				if ($_prod->getData('commission_percent') > 0)
				{
					$commis_percent = $_prod->getData('commission_percent');//commission_percent
				}
				else
				{
					$commis_percent = 0;
				}
				
				if ($_prod->getData('commission_fixed') > 0)
				{
					$commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
				}
				else
				{
					$commis_fixed = 0;
				}
				
				if ($_prod->getData('aammount') > 0)
				{
					$additional_ammount = $_prod->getData('aammount');//aammount
				}
				else
				{
					$additional_ammount = 0;
				}
				
				$sql = "SELECT merchant_id FROM groupdeals WHERE product_id = $ids";
				$res = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				$sql = "SELECT twitter FROM groupdeals_merchants WHERE merchants_id = $res";
				$freq = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				
			    $name = $item->getName();
			   	$commission = (100 - $commis_percent) / 100;
			    if ($commission != 1)
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount) * $commission;
			    }
			    else
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount)  - $commis_fixed;
			    }
			    $sku=$item->getSku();
			    
			    $qty=$item->getQtyOrdered();
			    
			    if ($res > 0 && $freq == '0||2')//daily report
			    {
			    	$product_order_details[$res][$ids]['name'] = $name;
			    	$product_order_details[$res][$ids]['ids'] = $ids;
			    	$product_order_details[$res][$ids]['unitPrice'] = $item->getPrice();
			    	$product_order_details[$res][$ids]['qty'] += $qty;
			    	$product_order_details[$res][$ids]['total_merchant'] += ($qty * $unitPrice);
			    	$product_order_details[$res][$ids]['total_rev'] += ($qty * $item->getPrice());
			    	
			    	//$product_order_details[$res][$ids]['gp'] = ($item->getPrice());
			    	$product_order_details[$res][$ids]['aa'] = ($qty * $additional_ammount);
			    	$product_order_details[$res][$ids]['com'] = ($commis_percent);
			    	$product_order_details[$res][$ids]['com_fixed'] = ($qty * $commis_fixed);
			    	
			    }
			}
			
		}
	  	$mage_csv = new Varien_File_Csv();
	 	$file_path = '/tmp/sample.csv';
		foreach($product_order_details as $merch_id=>$arr)
		{
			unset($products_row);
			$products_row[] = array('Product Name', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Total Revenue($)', 'Additional Ammount ($)', 'Commission Rate(%)','Commission Rate(fixed)', 'Net Payable to Merchant($)', 'Net Revenue iKoala($)');
			foreach ($arr as $key=>$value)
			{
				
			    $data = array();
			    $data[] = $value['name'];
			    $data[] = $value['ids'];
			    $data[] = $value['unitPrice'];
			    $data[] = $value['qty'];
			    $data[] = $value['total_rev'];
			    
			    
			    $data[] = $value['aa'];
			    $data[] = $value['com'];
			    $data[] = $value['com_fixed'];	
			    $data[] = $value['total_merchant'];
			    $data[] = $value['total_rev'] - $value['total_merchant'];
			    	
			    $products_row[] = $data;
			    
			}
			
			$sql = "SELECT email, name FROM groupdeals_merchants WHERE merchants_id = $merch_id";
			$email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
			//echo "<pre>";print_r($email_add);die();
			$email_ser = explode('||', $email_add[0]['email']);
			$email = $email_ser[1];
			
			$name_ser = explode('||', $email_add[0]['name']);
			$name = $name_ser[1];
			
			//echo $name;echo $email;die();
			$mage_csv->saveData($file_path, $products_row); 
			
			try
			{
				$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
				$mail = new Zend_Mail();
				$mail->setFrom("deals@ikoala.com.au","iKoala Team");
				$mail->addTo($email,$name);
				$mail->addCc('merchantsalesreports@ikoala.com.au','ikOala');//$email
				$mail->addCc('accounts@ikoala.com.au','ikOala');//$email
				$mail->setSubject("Daily Reports");
				$mail->setBodyHtml("Dear $name, <br/><br/> Please find attached the daily reports of your products. <br/><br/>Sincerly,<br/>iKoala Team"); // here u also use setBodyText options.
				 
				$fileContents = file_get_contents($file_path);
				$file = $mail->createAttachment($fileContents);
				$file->filename = "daily_reports_$today.csv";

				$mail->send();
 
			}
			catch(Exception $e)
			{
				echo $e->getMassage();
			}
			
		}
	}
	
	
	
	
	public function send_weekely_report(){

		$day = new DateTime();
		$day->modify( "-7 day" );
	 
		$fromDate = $day->format("Y-m-d 00:00:00");
	
		$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at>'$fromDate'";
		$orders = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
	
		foreach ($orders as $neworder)
		{
			
			$order_id = $neworder['order_id'];
			$order = Mage::getModel('sales/order')->load($order_id);
			$items = $order->getAllItems();
			
			$itemcount=count($items);
			$name=array();
			$unitPrice=array();
			$sku=array();
			$ids=array();
			$qty=array();
			foreach ($items as $itemId => $item)
			{
				
				$ids=$item->getProductId();
				$_prod = Mage::getModel('catalog/product')->load($ids);
				
				if ($_prod->getData('commission_percent') > 0)
				{
					$commis_percent = $_prod->getData('commission_percent');//commission_percent
				}
				else
				{
					$commis_percent = 0;
				}
				
				if ($_prod->getData('commission_fixed') > 0)
				{
					$commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
				}
				else
				{
					$commis_fixed = 0;
				}
				
				if ($_prod->getData('aammount') > 0)
				{
					$additional_ammount = $_prod->getData('aammount');//aammount
				}
				else
				{
					$additional_ammount = 0;
				}
				
				
				
				$sql = "SELECT merchant_id FROM groupdeals WHERE product_id = $ids";
				$res = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				$sql = "SELECT twitter FROM groupdeals_merchants WHERE merchants_id = $res";
				$freq = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql);
				
			    $name = $item->getName();
			    $commission = (100 - $commis_percent) / 100;
			    if ($commission != 1)
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount) * $commission;
			    }
			    else
			    {
			    	$unitPrice = ($item->getPrice() - $additional_ammount)  - $commis_fixed;
			    }
			    $sku=$item->getSku();
			    
			    $qty=$item->getQtyOrdered();
			    
			   
			    if ($res > 0 && 1 == 1)//weekely report, $freq == '0||1'
			    {
			    	$product_order_details[$res][$ids]['name'] = $name;
			    	$product_order_details[$res][$ids]['ids'] = $ids;
			    	$product_order_details[$res][$ids]['unitPrice'] = $item->getPrice();
			    	$product_order_details[$res][$ids]['qty'] += $qty;
			    	$product_order_details[$res][$ids]['total_merchant'] += ($qty * $unitPrice);
			    	$product_order_details[$res][$ids]['total_rev'] += ($qty * $item->getPrice());
			    	
			    	//$product_order_details[$res][$ids]['gp'] = ($item->getPrice());
			    	$product_order_details[$res][$ids]['aa'] = ($qty * $additional_ammount);
			    	$product_order_details[$res][$ids]['com'] = ($commis_percent);
			    	$product_order_details[$res][$ids]['com_fixed'] = ($qty * $commis_fixed);
			    	
			    }
			}
			
		}
		
	  	$mage_csv = new Varien_File_Csv();
	 	$file_path = '/tmp/sample.csv';
		foreach($product_order_details as $merch_id=>$arr)
		{
			unset($products_row);
			$products_row[] = array('Product Name', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Total Revenue($)', 'Additional Ammount ($)', 'Commission Rate(%)','Commission Rate(fixed)', 'Net Payable to Merchant($)', 'Net Revenue iKoala($)');
			foreach ($arr as $key=>$value)
			{
				
			    $data = array();
			    $data[] = $value['name'];
			    $data[] = $value['ids'];
			    $data[] = $value['unitPrice'];
			    $data[] = $value['qty'];
			    $data[] = $value['total_rev'];
			    
			    
			    $data[] = $value['aa'];
			    $data[] = $value['com'];
			    $data[] = $value['com_fixed'];	
			    $data[] = $value['total_merchant'];
			    $data[] = $value['total_rev'] - $value['total_merchant'];
			    	
			    $products_row[] = $data;
			    
			}
			
			$sql = "SELECT email, name FROM groupdeals_merchants WHERE merchants_id = $merch_id";
			$email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
			//echo "<pre>";print_r($email_add);die();
			$email_ser = explode('||', $email_add[0]['email']);
			$email = $email_ser[1];
			
			$name_ser = explode('||', $email_add[0]['name']);
			$name = $name_ser[1];
			
			//echo $name;echo $email;die();
			$mage_csv->saveData($file_path, $products_row); 
			
			try
			{
				$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
				$mail = new Zend_Mail();
				$mail->setFrom("deals@ikoala.com.au","iKoala Team");
				$mail->addTo($email,$name);
				$mail->addCc('merchantsalesreports@ikoala.com.au','ikOala');//$email
				$mail->addCc('accounts@ikoala.com.au','ikOala');//$email
				$mail->setSubject("Weekely Reports");
				$mail->setBodyHtml("Dear $name, <br/><br/> Please find attached the weekely reports of your products. <br/><br/>Sincerly,<br/>iKoala Team"); // here u also use setBodyText options.
				 
				$fileContents = file_get_contents($file_path);
				$file = $mail->createAttachment($fileContents);
				$file->filename = "weekely_reports_$today.csv";

				$mail->send();
 
			}
			catch(Exception $e)
			{
				echo $e->getMassage();
			}
			
		}
	}
	
	public function getLinks()
	{
		$store_array = array(5,3,6,4);
		foreach ($store_array as $store)
		{
			if ($store == 5)
			{
				$store_var = "?store=everyday";
			}
			elseif ($store == 3)
			{
				$store_var = "?store=kids";
			}
			elseif ($store == 6)
			{
				$store_var = "?store=business";
			}
			elseif ($store == 4)
			{
				$store_var = "?store=travel";
			}
			$collection = Mage::getModel('catalog/product')->getCollection()
	  		->addFieldToFilter('product_store', array('finset'=>$store))
	  		->setOrder('groupdeal_type','ASC')
	  		->addAttributeToSelect( '*' ) 
	  		->load();
	  		
	  		//echo "<pre>";print_r($collection);die();
	   		foreach ($collection as $collect)
	   		{		   		
	   			//$store_url[$store] = $this->getUrl('');
		   		$id = $collect->getId();	   		
		   		$_topprod = Mage::getModel('catalog/product')->load($id);
		   		if (($_topprod->getGroupdealStatus()==1) && ($_topprod->isSaleable()))
		   		{
		   			$topcat = $_topprod->getCategoryIds();  
	     			unset ($topcatname);
					foreach ($topcat as $value)
					{
						$_cate = Mage::getModel('catalog/category')->load($value);
						$topcatname[] = $_cate->getName();	
					}	
		   			/*if (in_array($_REQUEST['cityfilter'], $topcatname))
					{
						$store_url[$store] = $_topprod->getProductUrl();
						if ($_REQUEST['cityfilter'] && $_REQUEST['cityfilter']!="")
		                {
		                	$store_url[$store].="?city=".urlencode($_REQUEST['cityfilter'])."";
		                }
						break;
					}
					else*/
					if(in_array('National', $topcatname))
					{
						$store_url[$store] = "".$_topprod->getProductUrl()."$store_var";
						break;
					}
		   		}
	   		}
		}
		return $store_url;
	}
	
	
	public function create_pdf($data)
	{
		//$this->_isExport = true;
	
	//	echo "aaa";
	//	die();
		$pdf = new Zend_Pdf();
		$page = $pdf->newPage('1684: 1190:');//Zend_Pdf_Page::SIZE_A4_LANDSCAPE //'1684: 1190:'
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
		$page->setFont($font, 9);
		$width = $page->getWidth();
		$pageHeight = $page->getHeight();
		$i=0;
		foreach ($data as $row) {
			$b++;
			if ($line_height > 30)
			{
				$j = $line_height ;
			}	
			else 
			{
				$j+=30;
			}
			$line_height=0;
			
			$i = 10;   
			$f=0;         
			foreach ($row as $column)
			{
				$f++;
				if ($f == 3 )
				{ 
					$str_arr= str_split(strtolower($column),11);
				}
				elseif ($f==1 && $column == "Total payable to merchant($)")
				{
					$str_arr= str_split(strtolower($column),17);
				}
				elseif ($f==1)
				{
					$str_arr= str_split(strtolower($column),20);
				}
				else 
				{
					$str_arr= str_split(strtolower($column),28);
				}
				$height = $j;
			
				foreach ($str_arr as $string)
				{
				
					$page->drawText($string, $i, $pageHeight-$height);
					$height+=30;
				
				}
				if ($height > $line_height)
				{
					$line_height = $height;
				}
				
				if ($f == 1 ||$f == 2 ||$f == 9 || $f == 10 || $f == 11 || $f == 13 || $f == 14 ||$f == 15 )
				{
					$i+=80;
				}
				else 
				{
					$i+=115;
				}
				
			}    
			if ($b == 19)    
			{
				$pdf->pages[] = $page;
				
				$b = 0;
				$page = $pdf->newPage('1684: 1190:');//Zend_Pdf_Page::SIZE_A4_LANDSCAPE //'1684: 1190:'
				$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
				$page->setFont($font, 9);
				$width = $page->getWidth();
				$pageHeight = $page->getHeight();
				$i=0;
				$j= 0;
				$height=0;
				$line_height =0;
			}
		
		}
		//echo "<pre>";print_r($page);die();
		//echo "bbb";
		
			$pdf->pages[] = $page;
		
		$pdf->save('/tmp/test1.pdf');
		return $pdf->render();
	}
 
}
?>