<?php
class Adodis_Bestsellers_Block_Product_Bestsellers extends Mage_Catalog_Block_Product_List
{
		/*
	public $auto,$tot_prod;
	public function __construct()
	{
		$auto=Mage::getStoreConfig("adodis_bestsellers/adodis_general/adodis_bestsellers_auto");
		$tot_prod=Mage::getStoreConfig("adodis_bestsellers/adodis_general/adodis_bestsellers_totprod");
		parent::__construct();
	if($auto==1){
		$storeId = Mage::app()->getStore()->getId();
		$products = Mage::getResourceModel('reports/product_collection')
		->addOrderedQty()
		->addAttributeToSelect('*')
		->setStoreId($storeId)
		->addStoreFilter($storeId)
		->setOrder('ordered_qty', 'desc');

		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);

		$products->setPageSize($tot_prod)->setCurPage(1);

		$this->setProductCollection($products);
	}

  else{
		        $storeId    = Mage::app()->getStore()->getId();
		        $product    = Mage::getModel('catalog/product');
		        
		        $todayDate  = $product->getResource()->formatDate(time());
		        $products   = $product->setStoreId($storeId)->getCollection()
		        	->addAttributeToSelect('status')
		            ->addAttributeToFilter('bestsellers', array('Yes'=>true))
		            ->addAttributeToSelect(array('name', 'price', 'small_image'), 'inner')
		            ->addAttributeToSelect(array('special_price', 'special_from_date', 'special_to_date'), 'left');
		      
		        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
		        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
		      	$products->setOrder('hot_deals')->setPageSize($tot_prod)->setCurPage(1);
		        $this->setProductCollection($products);


  		}
		
}
*/
function get_prod_count()
   {
      //unset any saved limits
      Mage::getSingleton('catalog/session')->unsLimitPage();
      return (isset($_REQUEST['limit'])) ? intval($_REQUEST['limit']) : 12;
   }// get_prod_count

   function get_cur_page()
   {
      return (isset($_REQUEST['p'])) ? intval($_REQUEST['p']) : 1;
   }// get_cur_page

   /**
    * Retrieve loaded category collection
    *
    * @return Mage_Eav_Model_Entity_Collection_Abstract
   **/
   protected function _getProductCollection()
   {
      $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
      $storeId    = Mage::app()->getStore()->getId();
	$toolbar=$this->getToolbarBlock();
      $collection = Mage::getResourceModel('catalog/product_collection');
      $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
      $collection = $this->_addProductAttributesAndPrices($collection) 
     	 ->setStoreId($storeId)      
	  	->addAttributeToSelect('status')
            ->addAttributeToFilter('bestsellers', array('Yes'=>true))
            ->addAttributeToSelect(array('name', 'price', 'small_image'), 'inner')
            ->addAttributeToSelect(array('special_price', 'special_from_date', 'special_to_date'), 'left')
		  	->setOrder($_REQUEST['order'], $_REQUEST['dir']);
      $this->setProductCollection($collection);
	   $toolbar->setCollection($collection);
      return $collection;
   }
}