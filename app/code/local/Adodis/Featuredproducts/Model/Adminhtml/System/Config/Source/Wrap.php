<?php
class Adodis_Featuredproducts_Model_Adminhtml_System_Config_Source_Wrap
{

   public function toOptionArray()
    {
        return array(
            array('value' => 'first', 'label'=>Mage::helper('adminhtml')->__('Left to right')),
             array('value' => 'last', 'label'=>Mage::helper('adminhtml')->__('Right to Left')),
            array('value' => 'both', 'label'=>Mage::helper('adminhtml')->__('Both')),
            array('value' => 'circular', 'label'=>Mage::helper('adminhtml')->__('Circular')),
        );
    }

}