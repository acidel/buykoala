<?php
class Adodis_Featuredproducts_Model_Adminhtml_System_Config_Source_Displaytype
{

   public function toOptionArray()
    {
        return array(
            array('value' => 'carousel', 'label'=>Mage::helper('adminhtml')->__('Carousel')),
            array('value' => 'block', 'label'=>Mage::helper('adminhtml')->__('Block')),
        );
    }

}