<?php
class Adodis_Featuredproducts_Model_Adminhtml_System_Config_Source_Autoscroll
{

   public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Enabled')),
            array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('Disabled')),
        );
    }

}