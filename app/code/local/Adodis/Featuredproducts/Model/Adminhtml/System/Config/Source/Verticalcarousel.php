<?php
class Adodis_Featuredproducts_Model_Adminhtml_System_Config_Source_Verticalcarousel
{

   public function toOptionArray()
    {
        return array(
            array('value' => 'true', 'label'=>Mage::helper('adminhtml')->__('Yes')),
             array('value' => 'false', 'label'=>Mage::helper('adminhtml')->__('No')),
        );
    }

}