<?php
class Adodis_Featuredproducts_Block_Product_Featured extends Mage_Catalog_Block_Product_List
{ 
protected $categories;

  function __construct() {    
	$category = Mage::getModel('catalog/category')->load(55); //category id 55 for buy koala at local it is 56
	$this->categories=$category->getAllChildren(true);
   }


function get_prod_count()
   {
      //unset any saved limits
      Mage::getSingleton('catalog/session')->unsLimitPage();
      return (isset($_REQUEST['limit'])) ? intval($_REQUEST['limit']) : 12;
   }// get_prod_count

   function get_cur_page()
   {
      return (isset($_REQUEST['p'])) ? intval($_REQUEST['p']) : 1;
   }// get_cur_page

   /**
    * Retrieve loaded category collection
    *
    * @return Mage_Eav_Model_Entity_Collection_Abstract
   **/
   protected function _getProductCollection()
   {	
	$toolbar=$this->getToolbarBlock();
      $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		$storeId    = Mage::app()->getStore()->getId();
      $collection = Mage::getResourceModel('catalog/product_collection');
      $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
      $collection = $this->_addProductAttributesAndPrices($collection)  ->setStoreId($storeId)    
	  				->addAttributeToSelect('status')
					->addAttributeToFilter('featured_products', array('Yes'=>true))	
					->joinField('category_id',
					 'catalog/category_product',
					  'category_id',
					   'product_id=entity_id',
					   null,
				  'inner')		   
			->addAttributeToFilter('category_id', array('in' => $this->categories))
            ->addAttributeToSelect(array('name', 'price', 'small_image'), 'inner')
            ->addAttributeToSelect(array('special_price', 'special_from_date', 'special_to_date'), 'left')
		  	->setOrder($_REQUEST['order'], $_REQUEST['dir']);	
	 $collection->getSelect()->group('e.entity_id');	
      $this->setProductCollection($collection);
	  $toolbar->setCollection($collection);
	 // var_dump($collection->getSelect());
	// die(print_r((string)$collection->getSelect()));
      return $collection;
   }
}