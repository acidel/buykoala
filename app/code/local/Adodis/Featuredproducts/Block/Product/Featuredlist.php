<?php
class Adodis_Featuredproducts_Block_Product_Featuredlist extends Mage_Catalog_Block_Product_Abstract
{ public $totproduct,$opt;
protected $categories;
	public function __construct()
    {
	$category = Mage::getModel('catalog/category')->load(55); //category id 55 for buy koala at local it is 56
	$this->categories=$category->getAllChildren(true);
	$totproduct=Mage::getStoreConfig("adodis_featuredproducts/adodis_block/adodis_featuredproduct_block_product_number");    	$opt=Mage::getStoreConfig('adodis_featuredproducts/adodis_general/adodis_featuredproducts_display_type');
        parent::__construct();
        $storeId    = Mage::app()->getStore()->getId();        $product    = Mage::getModel('catalog/product');
        /* @var $product Mage_Catalog_Model_Product */
        $todayDate  = $product->getResource()->formatDate(time());
        $products   = $product->setStoreId($storeId)->getCollection()
        	->addAttributeToSelect('status')
            ->addAttributeToFilter('featured_products', array('Yes'=>true))
			->addAttributeToFilter('featured_products', array('Yes'=>true))	
					->joinField('category_id',
					 'catalog/category_product',
					  'category_id',
					   'product_id=entity_id',
					   null,
				  'inner')		   
			->addAttributeToFilter('category_id', array('in' => $this->categories))
            ->addAttributeToSelect(array('name', 'price', 'small_image'), 'inner')
            ->addAttributeToSelect(array('special_price', 'special_from_date', 'special_to_date'), 'left');
		$products->getSelect()->group('e.entity_id');
       /* @var $products Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);      if($opt=='block'){
      $products->setOrder('hot_deals')->setPageSize($totproduct)->setCurPage(1);      }      else{      	//$products->setOrder('hot_deals')->setPageSize(4)->setCurPage(1);   
	  }
        $this->setProductCollection($products);
    }
}