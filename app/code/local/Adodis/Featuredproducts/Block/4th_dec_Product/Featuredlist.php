<?php
class Adodis_Featuredproducts_Block_Product_Featuredlist extends Mage_Catalog_Block_Product_Abstract
{ public $totproduct,$opt;
	public function __construct()
    {    	$totproduct=Mage::getStoreConfig("adodis_featuredproducts/adodis_block/adodis_featuredproduct_block_product_number");    	$opt=Mage::getStoreConfig('adodis_featuredproducts/adodis_general/adodis_featuredproducts_display_type');
        parent::__construct();
        $storeId    = Mage::app()->getStore()->getId();        $product    = Mage::getModel('catalog/product');
        /* @var $product Mage_Catalog_Model_Product */
        $todayDate  = $product->getResource()->formatDate(time());
        $products   = $product->setStoreId($storeId)->getCollection()
        	->addAttributeToSelect('status')
            ->addAttributeToFilter('featured_products', array('Yes'=>true))
            ->addAttributeToSelect(array('name', 'price', 'small_image'), 'inner')
            ->addAttributeToSelect(array('special_price', 'special_from_date', 'special_to_date'), 'left');
       /* @var $products Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);      if($opt=='block'){
      $products->setOrder('hot_deals')->setPageSize($totproduct)->setCurPage(1);      }      else{      	//$products->setOrder('hot_deals')->setPageSize(4)->setCurPage(1);   
	  }
        $this->setProductCollection($products);
    }
}