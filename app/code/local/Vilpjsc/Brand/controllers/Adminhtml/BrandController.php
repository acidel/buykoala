<?php

/**
 * Vilpjsc Brand Admin brand controller
 *
 * @category   Vilpjsc
 * @package    Vilpjsc_Brand
 * @author     An <an@Vilpjscusa.com>
 */
class Vilpjsc_Brand_Adminhtml_BrandController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('brand/items')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Brand Names Manager'), Mage::helper('adminhtml')->__('Brand Names Manager'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction()->renderLayout();
    }

    public function editAction() {
        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setCodeFilter('manufacturer')->getFirstItem();
        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setPositionOrder('asc')
                ->setAttributeFilter($attributeInfo->getAttributeId())
                ->setStoreFilter(Mage::app()->getStore()->getId());
        $id = $this->getRequest()->getParam('manufacturer_option_id');
        $model = Mage::getModel('brand/brand')->load($id, 'manufacturer_option_id');

        if ($model->getManufacturerOptionId()) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('brand_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('brand/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brand Names Manager'), Mage::helper('adminhtml')->__('Brand Names Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brand Name'), Mage::helper('adminhtml')->__('Brand Name'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('brand/adminhtml_brand_edit'))
                    ->_addLeft($this->getLayout()->createBlock('brand/adminhtml_brand_edit_tabs'));

            $this->renderLayout();
        } else {
            $data = $collection->getItemByColumnValue("option_id", $id);
            if ($data) {
                $data->getData();
                $model->setTitle($data['value']);
                $model->setStatus(2);
                $model->setContent("Logo of " . $data['value']);
                $model->setFilename("");
                $model->setManufacturerOptionId($data['option_id']);
            }

            Mage::register('brand_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('brand/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brand Name Manager'), Mage::helper('adminhtml')->__('Brand Names Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brand Name'), Mage::helper('adminhtml')->__('Brand Name'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('brand/adminhtml_brand_edit'))
                    ->_addLeft($this->getLayout()->createBlock('brand/adminhtml_brand_edit_tabs'));

            $this->renderLayout();
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {

            $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                            ->setCodeFilter('manufacturer')->getFirstItem();
            $option['attribute_id'] = $attributeInfo->getAttributeId();
            $option['value'][0][Mage::app()->getStore()->getId()] = $data['title'];
            $option['value'][0][1] = $data['title'];
            $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setPositionOrder('asc')
                    ->setAttributeFilter($attributeInfo->getAttributeId())
                    ->setStoreFilter(Mage::app()->getStore()->getId());
            $exist = $collection->getItemByColumnValue("value", $data['title']);
            //this way the name is saved in DB
            $data['filename'] = $_FILES['filename']['name'];
            if (!$exist) {
                $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
                $setup->addAttributeOption($option);
                $newOption = $newcollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                                ->setPositionOrder('asc')
                                ->setAttributeFilter($attributeInfo->getAttributeId())
                                ->setStoreFilter(Mage::app()->getStore()->getId())->getItemByColumnValue("value", $data['title']);
                $model = Mage::getModel('brand/brand');
                $model->setData($data)
                        ->setManufacturerOptionId($newOption['option_id']);
            } else {
                $model = Mage::getModel('brand/brand');
                $model->setData($data)->setId($this->getRequest()->getParam('id'))
                        ->setManufacturerOptionId($exist['option_id']);
            }

            try {
                $model->save();
				//print_r($_FILES);die;
				
                if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                    try {
                        /* Starting upload */
                        $uploader = new Varien_File_Uploader('filename');
						
						//print_r($uploader);die;
                        // Any extension would work
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);

                        // Set the file upload mode
                        // false -> get the file directly in the specified folder
                        // true -> get the file in the product like folders
                        //	(file.jpg will go in something like /media/f/i/file.jpg)
                        $uploader->setFilesDispersion(true);

                        // We set media as the upload dir
                        $path = Mage::getBaseDir('media') . DS . 'brand' . DS;
                        preg_match('/\.([^\.]+)$/', $_FILES['filename']['name'], $extension);
                        Mage::helper('brand')->deleteBrandImage($model);
                        // if resized image doesn't exist, save the resized image to the resized directory
                        $uploader->save($path, $model->getId().$data['title'].strtolower($extension[0]));
                    } catch (Exception $e) {
                        
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('brand')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('manufacturer_option_id' => $model->getManufacturerOptionId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('manufacturer_option_id' => $this->getRequest()->getParam('manufacturer_option_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('brand')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function installAction() {
        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setCodeFilter('manufacturer')
                ->getFirstItem();

        $collectionTable = $attributeInfo->getSource();

        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setPositionOrder('asc')
                ->setAttributeFilter($collectionTable->getAttribute()->getId())
                ->setStoreFilter($collectionTable->getAttribute()->getStoreId())
                ->load();
        foreach ($collection->getData() as $item) {
            $data = array("title" => $item['value'], "status" => "2", "content" => "Logo of " . $item['value'], "brand_id" => $item['option_id'], "filename" => "");
            $model = Mage::getModel('brand/brand');
            $model->setData($data);
            try {
                Mage::getModel('brand/brand')->save($model);
            } catch (Exception $e) {
                echo "<pre>";
                print_r($e);
                echo "</pre>";
            }
            print_r($model->getData());
        }
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('manufacturer_option_id') > 0) {
            try {
                $query = Mage::getSingleton('core/resource')->getConnection('core_write');
                $tableName = Mage::getSingleton('core/resource')->getTableName('eav_attribute_option_value');
                $sql = "DELETE FROM " . $tableName . " WHERE " . $tableName . ".`option_id` = " . $this->getRequest()->getParam('manufacturer_option_id');
                $query->multi_query($sql);
                $tableNameOption = Mage::getSingleton('core/resource')->getTableName('eav_attribute_option');
                $sqlOption = "DELETE FROM " . $tableNameOption . " WHERE " . $tableNameOption . ".`option_id` = " . $this->getRequest()->getParam('manufacturer_option_id');
                $query->multi_query($sqlOption);
                $model = Mage::getModel('brand/brand')->load($this->getRequest()->getParam('manufacturer_option_id'), 'manufacturer_option_id');
                if ($model->getManufacturerOptionId()) {
                    $model->delete();
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Item could not be deleted'));
                $this->_redirect('*/*/');
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction() {
        $brandIds = $this->getRequest()->getParam('brand');
        if (!is_array($brandIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($brandIds as $brandId) {
                    $query = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $tableName = Mage::getSingleton('core/resource')->getTableName('eav_attribute_option_value');
                    $sql = "DELETE FROM " . $tableName . " WHERE " . $tableName . ".`option_id` = " . $brandId;
                    $query->multi_query($sql);
                    $tableNameOption = Mage::getSingleton('core/resource')->getTableName('eav_attribute_option');
                    $sqlOption = "DELETE FROM " . $tableNameOption . " WHERE " . $tableNameOption . ".`option_id` = " . $brandId;
                    $query->multi_query($sqlOption);
                    $model = Mage::getModel('brand/brand')->load($brandId, 'manufacturer_option_id');
                    if ($model->getManufacturerOptionId()) {
                        $model->delete();
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($brandIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction() {
        $brandIds = $this->getRequest()->getParam('brand');
        if (!is_array($brandIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($brandIds as $brandId) {
                    $brand = Mage::getSingleton('brand/brand')
                            ->load($brandId, 'manufacturer_option_id')
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($brandIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction() {
        $fileName = 'brand.csv';
        $content = $this->getLayout()->createBlock('brand/adminhtml_brand_grid')
                ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction() {
        $fileName = 'brand.xml';
        $content = $this->getLayout()->createBlock('brand/adminhtml_brand_grid')
                ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

}