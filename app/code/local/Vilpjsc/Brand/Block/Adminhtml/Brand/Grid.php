<?php

class Vilpjsc_Brand_Block_Adminhtml_Brand_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public $itemCount = 1;
    public function __construct() {
        $this->setId('newsGrid');
        $this->setDefaultSort('status');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        parent::__construct();
    }

    protected function _prepareCollection() {

        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setCodeFilter('manufacturer')->getFirstItem();
        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                        ->setPositionOrder('asc')
                        ->setAttributeFilter($attributeInfo->getAttributeId())
                        ->setStoreFilter(Mage::app()->getStore()->getId());
        /* $collection = Mage::getModel('brand/brand')->getCollection(); */
        $collection->getSelect()->joinLeft(
                array('brand' => $collection->getTable('brand/brand')),
                'brand.manufacturer_option_id = main_table.option_id',
                array('brand.status as brand_status', 'brand.filename as logo', 'brand.content as brand_content','brand.brand_id as brand_id','brand.title as title')
        );
        $this->setCollection($collection);

//        var_dump(
//        	(string)$collection->getSelect()
//        );exit;
        
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('option_id', array(
            'header' => Mage::helper('brand')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'option_id'
        ));
        $this->addColumn('title', array(
            'header' => Mage::helper('brand')->__('Title'),
            'align' => 'left',
            'index' => 'value'
        ));
        $this->addColumn('brand_content', array(
            'header' => Mage::helper('brand')->__('Content'),
            'align' => 'left',
            'index' => 'brand_content',
        ));
       /* $this->addColumn('logo', array(
            'header' => Mage::helper('brand')->__('Logo'),
            'align' => 'left',
            'index' => 'logo',
            'renderer' => new Vilpjsc_Brand_Block_Adminhtml_Brand_Grid_Renderer_Image(),
        ));*/
        $this->addColumn('status', array(
            'header' => Mage::helper('brand')->__('Featured'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'brand_status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));

        $this->addColumn('action',
                array(
                    'header' => Mage::helper('brand')->__('Action'),
                    'width' => '100',
                    'type' => 'action',
                    'getter' => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('brand')->__('Delete'),
                        'url'       => array('base'=> '*/*/delete'),
                        'field'     => 'manufacturer_option_id',
                        'confirm'  => Mage::helper('brand')->__('Are you sure?')
                    )
                ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('brand')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('brand')->__('XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('manufacturer_option_id' => $row->getID()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('brand_id');
        $this->getMassactionBlock()->setFormFieldName('brand');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('brand')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('brand')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('brand/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('brand')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('brand')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
//        $categories = Mage::getSingleton('brand/categoryname')->getOptionArray();
//        array_unshift($categories, array('label'=>'', 'value'=>''));
//        $this->getMassactionBlock()->addItem('categoryname', array(
//             'label'=> Mage::helper('brand')->__('Attache Categories'),
//             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
//             'additional' => array(
//                    'visibility' => array(
//                         'name' => 'categoryname',
//                         'type' => 'select',
//                         'class' => 'required-entry',
//                         'label' => Mage::helper('brand')->__('Category List'),
//                         'values' => $categories
//                     )
//             )
//        ));
        return $this;
    }
}
