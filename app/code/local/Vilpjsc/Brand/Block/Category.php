<?php

class Vilpjsc_Brand_Block_Category extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getBrandCategories($categoryId) {
        $collection = Mage::getModel("brand/category")->getCollection()
                ->addFieldToFilter("entity_id",array("eq"=>$categoryId));
        $collection->getSelect()->joinLeft(
                array('brand' => $collection->getTable('brand/brand')),
                'brand.manufacturer_option_id = main_table.brand_id','brand.status = 1',
                array('brand.title as title', 'brand.filename as logo', 'brand.content as content')
        );
        return $collection;
    }

}