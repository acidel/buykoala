<?php

/**
 * Vilpjsc gallery Status Model
 *
 * @category   Vilpjsc
 * @package    Vilpjsc_gallery
 * @author     An <an@Vilpjscusa.com>
 */
class Vilpjsc_Brand_Model_Status extends Varien_Object
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED => Mage::helper('brand')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('brand')->__('Disabled')
        );
    }

}