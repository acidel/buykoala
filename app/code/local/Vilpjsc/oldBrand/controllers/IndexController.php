<?php

class Vilpjsc_Brand_IndexController extends Mage_Core_Controller_Front_Action
{
	//Index Page of Shop by brand's module
    public function indexAction()
    {
    	Mage::register('brand_key', $this->getRequest());
		$brand = Mage::getModel("brand/brand");
        Mage::register('brand', $brand);
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle('Shop By brand');
        $this->renderLayout();
    }
    //View Each brand name
    public function viewAction()
    {
        Mage::register('brand_key', $this->getRequest());
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper("brand")->__("All products of ").preg_replace("/%20/", " ", Mage::app()->getRequest()->getParam('brandname')))
                ->setKeywords(Mage::app()->getRequest()->getParam('brandname').Mage::helper("brand")->__("brand name"))
                ->setDescription(Mage::helper("brand")->__("All products of ").preg_replace("/%20/", " ", Mage::app()->getRequest()->getParam('brandname')));
        $this->renderLayout();
    }

} // end of class