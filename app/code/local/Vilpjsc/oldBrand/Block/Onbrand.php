<?php
class Vilpjsc_Brand_Block_Onbrand extends Mage_Catalog_Block_Product_List
{
    /*function _getProductCollection()
    {
        $visibility = array(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG);

        $urlKey = Mage::app()->getStore()->getCode();
        
        $_featcategory = Mage::getResourceModel('catalog/category_collection')
                      ->addAttributeToSelect('entity_id')
                      ->addAttributeToFilter('url_key', $urlKey)
                      ->getFirstItem();
        
        $this->_productCollection = Mage::getResourceModel('catalog/product_collection')->addAttributeToSelect('*')
        ->addAttributeToFilter('visibility', $visibility)->addCategoryFilter($_featcategory)
        //->addFieldToFilter(array(array('attribute'=>'is_on_sale','eq'=>1)))
        //->addAttributeToFilter('is_on_sale', array('null'=>true), 'left')
        ->setPageSize(9);
        
        return $this->_productCollection;
        
    }
    */
    protected function _toHtml()
    {
        $html = parent::_toHtml();
        $html .= '
        <script type="text/javascript">
        document.observe("dom:loaded", function() {
          // initially hide all containers for tab content
          $$("div.std").invoke("hide");
          $$("div.page-title").invoke("hide");
          
        });
        </script>
        '; 
        return $html;
    }
    protected function _getProductCollection()
    {
        $layer = Mage::getSingleton('catalog/layer');
        $rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
		$_featcategory = Mage::getModel('catalog/category')->load($rootCategoryId);
        $layer->setCurrentCategory($_featcategory);

        $layer = $this->getLayer();
        $layer->setCurrentCategory($_featcategory);
        Mage::App()->getRequest(); 
        $this->_productCollection = Mage::getModel('catalog/product')->getCollection()
        ->addAttributeToFilter('manufacturer', array("eq", Mage::App()->getRequest()->getParam('manufacturer')));
        $layer->prepareProductCollection($this->_productCollection);
        return $this->_productCollection;
    }
}