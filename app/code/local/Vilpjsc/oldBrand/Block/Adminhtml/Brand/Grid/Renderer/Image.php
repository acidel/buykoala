<?php
class Vilpjsc_Brand_Block_Adminhtml_Brand_Grid_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public $currentItem = 0;

    public function render(Varien_Object $row)
    {
        return '<img width="32" src="'.str_replace("index.php","", Mage::getBaseUrl()).'media/brand/'.$row->getData($this->getColumn()->getIndex()).'"/>';
    }

}
