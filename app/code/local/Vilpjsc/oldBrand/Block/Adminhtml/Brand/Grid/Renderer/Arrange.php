<?php
class Vilpjsc_Brand_Block_Adminhtml_Brand_Grid_Renderer_Arrange extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public $currentItem = 0;

    public function render(Varien_Object $row)
    {
        $this->currentItem++;
        return $this->currentItem;
    }

}
