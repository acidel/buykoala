<?php

/**
 * Vilpjsc Brand Category Model
 *
 * @category   Vilpjsc
 * @package    Vilpjsc_Brand
 * @author     An <an@Vilpjscusa.com>
 */
class Vilpjsc_Brand_Model_Mysql4_Category_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {


    public function _construct() {
        parent::_construct();
        $this->_init('brand/category');
    }

}