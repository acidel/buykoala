<?php

class Tis_Productfaq_Block_Adminhtml_Productfaq_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('productfaqGrid');
      //$this->setDefaultSort('productfaq_id');
      $this->setDefaultSort('created_on');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }
 
  protected function _getStore()
  {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
  }

  protected function _prepareCollection()
  {
//      $collection = Mage::getModel('productfaq/productfaq')->getCollection();
//      $this->setCollection($collection);
//      return parent::_prepareCollection();

  	
  	
        $store = $this->_getStore();

        $collection = Mage::getModel('productfaq/productfaq')
                            ->getCollection()
                            ->joinProducts()
                            ->setOrder('answered_on', 'ASC')
                            ->setOrder('created_on', 'ASC')
          ;

        if ($store->getId()) {
            $collection->addFieldToFilter('main_table.store_id', $store->getId());
        }

        //echo $collection->getSelectSql();

        $this->setCollection($collection);
        $columnId = $this->getParam($this->getVarNameSort(), $this->_defaultSort);

        // make it possible to sort by product_name
        if ($columnId == 'product_name' ) {
            $this->_preparePage();

            $columnId = $this->getParam($this->getVarNameSort(), $this->_defaultSort);
            $dir      = $this->getParam($this->getVarNameDir(), $this->_defaultDir);


            if (isset($this->_columns[$columnId]) && $this->_columns[$columnId]->getIndex()) {
                $dir = (strtolower($dir)=='desc') ? 'desc' : 'asc';
                $this->_columns[$columnId]->setDir($dir);
                $collection->getSelect()->order('IFNULL(_table_product_name.value,_table_product_name2.value) '.$dir);
            }

        } 

        parent::_prepareCollection();

        return $this;
    
  	
  }

  protected function _prepareColumns()
  {
      
  	
  	 $this->addColumn('productfaq_id', array(
          'header'    => Mage::helper('productfaq')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'productfaq_id',
      ));
      
      $this->addColumn('product_name', array(
            'header'    => Mage::helper('productfaq')->__('Product Name'),
            'align'     =>'left',
            'index'     => 'product_name',
            'filter_condition_callback' => array($this, '_filterProductNameCondition'),
      		'width'	=>	'150px',
        ));
        

      $this->addColumn('name', array(
          'header'    => Mage::helper('productfaq')->__('Name'),
          'align'     =>'left',
          'index'     => 'name',
      	
      ));

	  
      $this->addColumn('question', array(
			'header'    => Mage::helper('productfaq')->__('Person question'),
			'width'     => '300px',
			'index'     => 'question',
      ));
      
      $this->addColumn('email', array(
			'header'    => Mage::helper('productfaq')->__('Person E-mail'),
			'width'     => '150px',
			'index'     => 'email',
      ));
      
      $this->addColumn('created_on', array(
            'header'    => Mage::helper('productfaq')->__('Added On'),
            'align'     =>'left',
            'type'		=> 'datetime',
            'index'     => 'created_on',
      		'width'	=>	'155px',
        ));
      
      $this->addColumn('answered_on', array(
            'header'    => Mage::helper('productfaq')->__('Answered On'),
            'align'     =>'left',
            'index'     => 'answered_on',
            'type'		=> 'datetime',
      	'width'	=>	'155px',
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('productfaq')->__('Status'),
            'align'     =>'left',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array('public'=>'public','hidden'=>'hidden'),
        ));
	  

//      $this->addColumn('status', array(
//          'header'    => Mage::helper('productfaq')->__('Status'),
//          'align'     => 'left',
//          'width'     => '80px',
//          'index'     => 'status',
//          'type'      => 'options',
//          'options'   => array(
//              1 => 'Enabled',
//              2 => 'Disabled',
//          ),
//      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('productfaq')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('productfaq')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('productfaq')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('productfaq')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('productfaq_id');
        $this->getMassactionBlock()->setFormFieldName('productfaq');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('productfaq')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('productfaq')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('productfaq/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('productfaq')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('productfaq')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}