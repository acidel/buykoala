<?php

class Tis_Productfaq_Block_Adminhtml_Productfaq_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('productfaq_form', array('legend'=>Mage::helper('productfaq')->__('Item information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('productfaq')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

      $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('productfaq')->__('E-mail'),
          'required'  => false,
          'name'      => 'email',
	  ));
	  
//	  $fieldset_question->addField('product_name', 'text', array(
//            'name'      => 'record[product_name]',
//            'label'     => Mage::helper('productfaq')->__('Product'),
//            'readonly' => true,
//
//        ));
		
	  
	  
//      $fieldset->addField('status', 'select', array(
//          'label'     => Mage::helper('productfaq')->__('Status'),
//          'name'      => 'status',
//          'values'    => array(
//              array(
//                  'value'     => 1,
//                  'label'     => Mage::helper('productfaq')->__('Enabled'),
//              ),
//
//              array(
//                  'value'     => 2,
//                  'label'     => Mage::helper('productfaq')->__('Disabled'),
//              ),
//          ),
//      ));
//     
//      $fieldset->addField('content', 'editor', array(
//          'name'      => 'content',
//          'label'     => Mage::helper('productfaq')->__('Content'),
//          'title'     => Mage::helper('productfaq')->__('Content'),
//          'style'     => 'width:700px; height:500px;',
//          'wysiwyg'   => false,
//          'required'  => true,
//      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getProductfaqData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getProductfaqData());
          Mage::getSingleton('adminhtml/session')->setProductfaqData(null);
      } elseif ( Mage::registry('productfaq_data') ) {
          $form->setValues(Mage::registry('productfaq_data')->getData());
      }
      
//  		if(Mage::getSingleton('adminhtml/session')->getRecordData()){	
//		    $record = Mage::getSingleton('adminhtml/session')->getRecordData();		
//        	$form->setValues($record['record']);
//        	Mage::getSingleton('adminhtml/session')->setRecordData(false);
//        } elseif(Mage::registry('productfaq_data')) {
//            $form->setValues(Mage::registry('productfaq_data')->getData());
//        }
      
      
      return parent::_prepareForm();
  }
}