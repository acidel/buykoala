<?php

class Tis_Productfaq_Block_Adminhtml_Productfaq_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'productfaq';
        $this->_controller = 'adminhtml_productfaq';
        
        $this->_updateButton('save', 'label', Mage::helper('productfaq')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('productfaq')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('productfaq_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'productfaq_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'productfaq_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('productfaq_data') && Mage::registry('productfaq_data')->getId() ) {
            return Mage::helper('productfaq')->__("Edit Answer '%s'", $this->htmlEscape(Mage::registry('productfaq_data')->getTitle()));
        } else {
            return Mage::helper('productfaq')->__('Add Item');
        }
    }
}