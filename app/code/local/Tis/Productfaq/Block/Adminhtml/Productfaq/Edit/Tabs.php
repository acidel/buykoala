<?php

class Tis_Productfaq_Block_Adminhtml_Productfaq_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('productfaq_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('productfaq')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('productfaq')->__('Item Information'),
          'title'     => Mage::helper('productfaq')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('productfaq/adminhtml_productfaq_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}