<?php

class Tis_Productfaq_Block_Adminhtml_Productfaq_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      
      $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
      
      $fieldset = $form->addFieldset('productfaq_form', array('legend'=>Mage::helper('productfaq')->__('Item information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('productfaq')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

      $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('productfaq')->__('E-mail'),
          'required'  => false,
          'name'      => 'email',
	  ));
	  
	  $fieldset->addField('product_name', 'text', array(
            'name'      => 'product_name',
            'label'     => Mage::helper('productfaq')->__('Product'),
            'readonly' => true,

        ));
        
         $fieldset->addField('store_name', 'text', array(
            'name'      => 'store_name',
            'label'     => Mage::helper('productfaq')->__('Store'),
            'readonly' => true,

        ));
        
        $fieldset->addField('created_on', 'date', array(
            'name'      => 'created_on',
            'label'     => Mage::helper('productfaq')->__('Posted On'),
            'readonly' => true,
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso,
        ));
        
        $fieldset->addField('question', 'textarea', array(
          'name'      => 'question',
          'label'     => Mage::helper('productfaq')->__('Question'),
          'title'     => Mage::helper('productfaq')->__('Question'),
          'style'     => 'width:700px; height:250;',
          'wysiwyg'   => false,
          'required'  => true,
      	  'readonly'	=> true,
      ));
      
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('productfaq')->__('Status'),
          'name'      => 'status',
          'value'	  => 'public',
          'values'    => array(
              array(
                  'value'     => 'public',
                  'label'     => Mage::helper('productfaq')->__('public'),
              ),

              array(
                  'value'     => 'hidden',
                  'label'     => Mage::helper('productfaq')->__('hidden'),
              ),
          ),
          'readonly'	=>	'true',
      ));    
        
        $fieldset = $form->addFieldset('productfaq_form_answer', array(
            'legend'=>Mage::helper('productfaq')->__('Answer')
        ));    

        $fieldset->addField('answer', 'textarea', array(
            'name'      => 'answer',
            'label'     => Mage::helper('productfaq')->__('Answer'),
            'class'     => 'required-entry',
            'required'  => true,
        ));
		
	
      if ( Mage::getSingleton('adminhtml/session')->getProductfaqData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getProductfaqData());
          Mage::getSingleton('adminhtml/session')->setProductfaqData(null);
      } elseif ( Mage::registry('productfaq_data') ) {
          $form->setValues(Mage::registry('productfaq_data')->getData());
      }
      
      
      return parent::_prepareForm();
  }
  
	
}