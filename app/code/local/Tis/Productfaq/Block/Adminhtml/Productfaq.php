<?php
class Tis_Productfaq_Block_Adminhtml_Productfaq extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_productfaq';
    $this->_blockGroup = 'productfaq';
    $this->_headerText = Mage::helper('productfaq')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('productfaq')->__('Add Item');
    parent::__construct();
  }
}