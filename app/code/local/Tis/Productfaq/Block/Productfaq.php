<?php
class Tis_Productfaq_Block_Productfaq extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getProductfaq()     
     { 
        if (!$this->hasData('productfaq')) {
            $this->setData('productfaq', Mage::registry('productfaq'));
        }
        return $this->getData('productfaq');
        
    }
}