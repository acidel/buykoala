<?php
class Tis_Productfaq_Block_List extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
        
       $productfaqEntries = Mage::getModel('productfaq/productfaq')->getCollection()
            ->addFieldToFilter('status', 'public')
            ->addFieldToFilter('answered_on', array('notnull' => true))
            ->addFieldToFilter('product_id', Mage::registry('product')->getId())
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId())
            ->setOrder('created_on','DESC')
        ;

        $this->setEntries($productfaqEntries);

    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'productfaq.pager');
        $pager->setAvailableLimit(array(5=>5, 10=>10, 20=>20, 50=>50));    
        $pager->setCollection($this->getEntries());
        
        $this->setChild('pager', $pager);
        $this->getEntries()->load();
               
        
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
	
	public function testblockprint() {
		
		echo 'msg test block print';
	
	}

}
