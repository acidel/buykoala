<?php

class Tis_Productfaq_Model_Mysql4_Productfaq extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the productfaq_id refers to the key field in your database table.
        $this->_init('productfaq/productfaq', 'productfaq_id');
    }
}