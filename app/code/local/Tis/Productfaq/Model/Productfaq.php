<?php

class Tis_Productfaq_Model_Productfaq extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('productfaq/productfaq');
    }
    
    
    public function loadExtra($id)
    {
        $collection = $this->getCollection()
                            ->joinProducts()
                            ->joinStore()
                            ->addFieldToFilter('productfaq_id', $id);
        
        if ($collection->getSize()) {
            return $collection->getFirstItem();
        }  
        
        return false;
    }
    
    
    
}