<?php
class Tis_Productfaq_IndexController extends Mage_Core_Controller_Front_Action
{	
	
	const CONFIG_SEND_NOTIFICATION_EMAIL = 'productfaq/general/send_notification';
   	const CONFIG_SEND_NOTIFICATION_EMAIL_TO = 'productfaq/general/notification_email';
   
	const XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY  = 'default/productfaq/emails/email_identity';
    const XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE  = 'product_faq_notification';
    
   	
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/productfaq?id=15 
    	 *  or
    	 * http://site.com/productfaq/id/15 	
    	 */
    	/* 
		$productfaq_id = $this->getRequest()->getParam('id');

  		if($productfaq_id != null && $productfaq_id != '')	{
			$productfaq = Mage::getModel('productfaq/productfaq')->load($productfaq_id)->getData();
		} else {
			$productfaq = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($productfaq == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$productfaqTable = $resource->getTableName('productfaq');
			
			$select = $read->select()
			   ->from($productfaqTable,array('productfaq_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$productfaq = $read->fetchRow($select);
		}
		Mage::register('productfaq', $productfaq);
		*/
	
			
		$this->loadLayout();     
		$this->renderLayout();
    }
    
    
    public  function addFaqAction() {
    	
        if ($post = $this->getRequest()->getPost()) {
           
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);
				//var_dump($postObject->getData());
				//var_dump($postObject->getName());
				//exit;
                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['question']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

               
                if ($error) {
                    throw new Exception();
                }
                
                $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $now = Mage::getModel('core/date')->timestamp(time());
                $post['created_on'] = date('Y-m-d h:i:s', $now);

                $productfaq = Mage::getModel('productfaq/productfaq');
                $productfaq->setData($post);
                $productfaq->save();
                
                
                // --------------------- SEND NOTIFICATION EMAIL
                //Mage::getStoreConfig('EXAMPLE/SAMPLE/ENABLED');
            
            	$sendNotification = Mage::getStoreConfig('productfaq/general/send_notification');
            	$notificationEmail = Mage::getStoreConfig('productfaq/general/notification_email');
            	
            
                               
                 $sendNotificationEmail = Mage::getStoreConfig(self::CONFIG_SEND_NOTIFICATION_EMAIL);
               
                
                if ($sendNotificationEmail) {  
                    $product = Mage::getModel('catalog/product')->load($productfaq->getProductId());
                   
                    //var_dump($data);
                              
                    $emailData = array();
                    $emailData['to_email'] = $notificationEmail;
                    $emailData['to_name'] =  $productfaq->getName();
                    $emailData['email'] = array(
                        'product_name' => $product->getName(),
                        'store_id' => $productfaq->getStoreId(),
                        'question' => $productfaq->getQuestion(),
                        'date_posted' => Mage::helper('core')->formatDate($productfaq->getCreatedOn(), 'long'), 
                    ); 
					
					//var_dump($emailData);
					
		            $result = $this->sendEmail($emailData);
		            //echo '<pre>';
		            //print_r($result);
		            //exit;
		            if(!$result) {
		                Mage::throwException($this->__('Cannot send email'));
		            }
		        }               
                
				//exit;
                Mage::getSingleton('catalog/session')->addSuccess(Mage::helper('productfaq')->__('Thank You. We\'ll reply you as soon as possible'));
                
                return $this->_redirectReferer();
            } catch (Exception $e) {               

                Mage::getSingleton('catalog/session')->addError(Mage::helper('productfaq')->__('Unable to submit your question. Please, try again later'.$e->getMessage()));
                
                return $this->_redirectReferer();
            }

        } else {
            return $this->_redirectReferer();
        }
    }
    
	/*private function sendEmail($data)
	{	
		//var_dump($data) ;exit;
		 $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');
		 $mailSubject='Product Question Notificaiton';
		 $templateId = self::XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE;
		 $sender = $senderEmail;
		 $email=$data['to_email'];
		 $name=$data['to_name'];
		 $vars=$data['email'];
		 
		 $storeId=$data['email']['store_id'];;
		//var_dump($vars);
		
		
		$translate = Mage::getSingleton('core/translate');
  
        $translate->setTranslateInline(false);

		$resultMail = Mage::getModel('core/email_template')
	      ->setTemplateSubject($mailSubject)
	      ->sendTransactional($templateId, $sender, $email, $name, $vars, $storeId);
	      
	  	$translate->setTranslateInline(true);
	
		
		
        $resultMail = Mage::getModel('core/email_template')
         ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));
        
       $resultMail->sendTransactional(
                self::XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE,
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY),
                $senderEmail,
                $data['to_email'],
                $data['to_name'],
                $data['email'],
                $storeID
               );
       
	  	       
        //echo $result->getProcessedTemplate($data);       
        $translate->setTranslateInline(true);
        var_dump($resultMail);
        exit;
        //echo '<pre>';
        //print_r($result);
        //exit;
        return $resultMail;
	}*/
	
	
	
 	private function sendEmail($data)
	{	
		
		$storeID = $data['email']['store_id'];
		
		$translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $result = Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));
        
        $result->sendTransactional(
                self::XML_PATH_EMAIL_PRODUCT_NOTIFICATION_TEMPLATE,
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY),
                $data['to_email'],
                $data['to_name'],
                $data['email'],
                $storeID
               );
        //echo $result->getProcessedTemplate($data);       

        $translate->setTranslateInline(true);
        
        return $result;
	}
    
}