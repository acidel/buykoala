<?php

class Tis_Productfaq_Adminhtml_ProductfaqController extends Mage_Adminhtml_Controller_action
{
	const XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY  = 'default/productfaq/emails/email_identity';
	const XML_PATH_EMAIL_PRODUCT_QUESTION_TEMPLATE  = 'product_faq_answer';
	
	

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('productfaq/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		
		
		$id     = $this->getRequest()->getParam('id');
		$model = Mage::getModel('productfaq/productfaq');
		$model = $model->loadExtra($id);
	
		
		//$model  = Mage::getModel('productfaq/productfaq')->load($id);
				
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('productfaq_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('productfaq/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('productfaq/adminhtml_productfaq_edit'))
				->_addLeft($this->getLayout()->createBlock('productfaq/adminhtml_productfaq_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productfaq')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			
			$proModel = Mage::getModel('productfaq/productfaq')->load($this->getRequest()->getParam('id'));
			
			$model = Mage::getModel('productfaq/productfaq');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedOn == NULL || $model->getAnsweredOn() == NULL) {
					$model->setCreatedOn(now())
						->setAnsweredOn(now());
				} else {
					$model->setAnsweredOn(now());
				}	
				
				$model->save();
				
				$sendEmailToUser = Mage::getStoreConfig('productfaq/general/send_email');
                //echo $model->getAnswer();
                if ($sendEmailToUser) {  
                	              
                    $emailData = array();
                    $emailData['to_email'] = $model->getEmail();
                    $emailData['to_name'] = $model->getName();
                    $emailData['email'] = array(
                        'product_name' => $model->getProductName(),
                        //'store_id' => Mage::app()->getStore()->getId(),
                    	'store_id' =>  $proModel->getStoreId(),
                        'store_name' => $model->getStoreName(),
                        'question' => $model->getQuestion(),
                        'answer' => $model->getAnswer(),
                        'customer_name' => $model->getName(),
                        'date_posted' => Mage::helper('core')->formatDate($model->getCreatedOn(), 'long'), 
                    ); 
		            $result = $this->sendEmail($emailData);
		            //var_dump($result);
		            if(!$result) {
		                Mage::throwException($this->__('Cannot send email'));
		            }
		        } 
				//exit;
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('productfaq')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productfaq')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('productfaq/productfaq');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $productfaqIds = $this->getRequest()->getParam('productfaq');
        if(!is_array($productfaqIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($productfaqIds as $productfaqId) {
                    $productfaq = Mage::getModel('productfaq/productfaq')->load($productfaqId);
                    $productfaq->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($productfaqIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $productfaqIds = $this->getRequest()->getParam('productfaq');
        if(!is_array($productfaqIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($productfaqIds as $productfaqId) {
                    $productfaq = Mage::getSingleton('productfaq/productfaq')
                        ->load($productfaqId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($productfaqIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'productfaq.csv';
        $content    = $this->getLayout()->createBlock('productfaq/adminhtml_productfaq_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'productfaq.xml';
        $content    = $this->getLayout()->createBlock('productfaq/adminhtml_productfaq_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
    
	private function sendEmail($data)
	{	
		//var_dump($data);
		$storeID = $data['email']['store_id'];
		
		$translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $result = Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));
        
        $result->sendTransactional(
                self::XML_PATH_EMAIL_PRODUCT_QUESTION_TEMPLATE,
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_PRODUCT_QUESTION_IDENTITY),
                $data['to_email'],
                $data['to_name'],
                $data['email'],
                $storeID
               );
        //$result->getProcessedTemplate($data);       

        $translate->setTranslateInline(true);
        //var_dump($result);
        //exit;
        return $result;
	}
    
}