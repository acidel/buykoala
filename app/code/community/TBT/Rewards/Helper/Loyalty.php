<?php
include_once('app/code/community/TBT/Common/Helper/LoyaltyAbstract.php');
class TBT_Rewards_Helper_Loyalty extends TBT_Common_Helper_LoyaltyAbstract {
    
    public function getModuleKey() 
    {
        return 'rewards';
    }
    
    /*
     * TODO: Abstract these functions to tbtcommon for other modules to use
     */
    public function checkForUpdates() {
        try {
            $updates_response = $this->fetchUpdatesResponse();
    
            if(empty($updates_response)) {
                return $this;
            }
            
            $update_msg = (array) json_decode($updates_response);
            
            if(!isset($update_msg['title']) || !isset($update_msg['description'])) {
                return $this;
            }
            
            $message = Mage::getModel ( 'adminnotification/inbox' )->setData($update_msg)->save();
        } catch(Exception $e) {
            Mage::helper('rewards/debug')->notice("Failed to check for updates from WDCA server: ". $e->getMessage() . ": ". ((string) $e)  );
        }
        return $this;
        
    }
 
    public function fetchUpdatesResponse() {
        $url = "http://www.wdca.ca/cem/api/check_updates.php";
        
        $fields = array(
            'license_key'   => $this->getLicenseKey(), 
            'identifier'    => $this->getModuleId(),
            'mage_ver'      => Mage::getVersion(),
            'module_ver'    => (string) Mage::getConfig()->getNode('modules/TBT_Rewards/version')
        );
        
        $output = $this->fetchUpdateResponse($url, $fields);
        
        return $output;
    }
    
    public function fetchUpdateResponse($url, $fields) {
        //open connection
        $ch = curl_init();
        $userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)';
        
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        // user agent:
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
        
    }
    
    public function fetchValidationResponse($license) {
        return $this->isValidOverServer($license) ? 'license_valid' : 'invalid license';
    }
}