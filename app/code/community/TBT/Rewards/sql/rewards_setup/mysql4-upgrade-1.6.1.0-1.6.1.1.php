<?php
$installer = $this;

$installer->startSetup();


Mage::helper('rewards/mysql4_install')->attemptQuery($installer, "
	ALTER TABLE `{$this->getTable('sales_flat_quote_item')}`
		MODIFY COLUMN `row_total_before_redemptions` DECIMAL(12,4) DEFAULT NULL,
		MODIFY COLUMN `row_total_before_redemptions_incl_tax` DECIMAL(12,4) DEFAULT NULL,
		MODIFY COLUMN `row_total_after_redemptions` DECIMAL(12,4) DEFAULT NULL,
		MODIFY COLUMN `row_total_after_redemptions_incl_tax` DECIMAL(12,4) DEFAULT NULL
");

Mage::helper('rewards/mysql4_install')->attemptQuery($installer, "
	ALTER TABLE `{$this->getTable('sales_flat_order_item')}`
		MODIFY COLUMN `row_total_before_redemptions` DECIMAL(12,4) DEFAULT NULL,
		MODIFY COLUMN `row_total_before_redemptions_incl_tax` DECIMAL(12,4) DEFAULT NULL,
		MODIFY COLUMN `row_total_after_redemptions` DECIMAL(12,4) DEFAULT NULL,
		MODIFY COLUMN `row_total_after_redemptions_incl_tax` DECIMAL(12,4) DEFAULT NULL
");


$install_version = Mage::getConfig ()->getNode ( 'modules/TBT_Rewards/version' );
$msg_title = "Sweet Tooth v{$install_version} was successfully installed!";
$msg_desc = "Sweet Tooth v{$install_version} was successfully installed on your store.";
Mage::helper( 'rewards/mysql4_install' )->createInstallNotice( $msg_title, $msg_desc );

$installer->endSetup(); 
