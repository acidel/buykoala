<?php
/**
 *
 * @category   RedStage
 * @package    Who Viewed This Also Viewed
 * @author     amorris@redstage.com
 */
 
class RedStage_WhoViewedThisAlsoViewed_Adminhtml_Model_System_Config_Source_DefaultPosition
{
     public function toOptionArray()
    {
        return array(
            array('value' => 'right_column', 'label' => Mage::helper('whoviewedthisalsoviewed')->__('Right Column')),
            array('value' => 'main_column', 'label' => Mage::helper('whoviewedthisalsoviewed')->__('Main Column'))
        );
    }
}
