<?php
/**
 *
 * @category   RedStage
 * @package    Who Viewed This Also Viewed
 * @author     amorris@redstage.com
 */
class RedStage_WhoViewedThisAlsoViewed_Block_List extends Mage_Catalog_Block_Product_List
{
	
	public function getTimeLimit() 
    {
        return intval(Mage::getStoreConfig('whoviewedthisalsoviewed/general/session_lifetime'));
    }
	public function getProductLimit() 
    {
        return intval(Mage::getStoreConfig('whoviewedthisalsoviewed/general/products_to_diplay'));
    }
	public function isEnable() 
    {
        return intval(Mage::getStoreConfig('whoviewedthisalsoviewed/general/active'));
    }
	public function isShowFromOneCategory() 
    {
        return intval(Mage::getStoreConfig('whoviewedthisalsoviewed/general/show_product_one_category'));
    }
	public function getCount()  
    {
        return $this->_productCollection->count();
    }
	public function getDefaultPosition() 
    {
        return Mage::getStoreConfig('whoviewedthisalsoviewed/general/default_position');
    }
    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        $_storeId = Mage::app()->getStore()->getStoreId();
        $this->setStoreId($_storeId);
        if (is_null($this->_productCollection)) {
            $this->_productCollection = Mage::getResourceModel('reports/product_collection');
            if ($this->getTimeLimit()) {
                $_product = Mage::getModel('catalog/product');	
                $_todayDate = $_product->getResource()->formatDate(time());
                $_startDate = $_product->getResource()->formatDate(time() - 60 * $this->getTimeLimit());
                $this->_productCollection = $this->_productCollection->addViewsCount($_startDate, $_todayDate);
            } else {
                $this->_productCollection = $this->_productCollection->addViewsCount();
            }        
            $_statuses = Mage::getSingleton('catalog/product_status')->getVisibleStatusIds();
            $this->_productCollection = $this->_productCollection
                            ->setStoreId($_storeId)
                            ->addStoreFilter($_storeId)
                            ->addAttributeToFilter('status', $_statuses)
							->addAttributeToFilter('entity_id', array('nin' => array($this->getProductId())))
                            ->setPageSize($this->getProductLimit());	
			if($this->isShowFromOneCategory())
			{
				$_category = Mage::getModel('catalog/category')->load($this->getProductCategory());
				$this->_productCollection->addCategoryFilter($_category);
			}
			Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_productCollection, Mage::getSingleton('checkout/session')->getQuoteId());
		}		
        $this->_productCollection->load();
        return $this->_productCollection;
    }
	
    public function _toHtml()
    { 
        if ($this->_productCollection->count()) {
            return parent::_toHtml();
        } else {
            return '';
        }
    } 
	public function getProductId() {
   	   $_product = Mage::registry('product');
       return $_product->getId();
	}
	public function getProductCategory() {
   		$_product = Mage::registry('product');
        $_categoryIds = $_product->getCategoryIds();
        if (is_array($_categoryIds) && count($_categoryIds) > 0)
            return $_categoryIds[0];
		else
			return 0;
	}
    /**
     * Translate block sentence
     *
     * @return string
     */
    public function __()
    {
        $args = func_get_args();
        $expr = new Mage_Core_Model_Translate_Expr(array_shift($args), 'Mage_Catalog');
        array_unshift($args, $expr);
        return Mage::app()->getTranslator()->translate($args);
    }

}