<?php
class CMSFruit_UltimateLiveChat_Helper_Data extends Mage_Core_Helper_Abstract 
{
    public $_settings = array();

    public function getLiveChatSetting() 
    {        
        $this->_settings['hosted_mode_api_key'] = Mage::getStoreConfig('ultimate_livechat/settings/api_key_valid');       
        $this->_settings['popup_mode'] = Mage::getStoreConfig('ultimate_livechat/optional/popup_mode');
        $this->_settings['image_size'] = Mage::getStoreConfig('ultimate_livechat/optional/image_size');
        $this->_settings['specific_operators'] = Mage::getStoreConfig('ultimate_livechat/optional/specific_operators');
        $this->_settings['specific_department'] = Mage::getStoreConfig('ultimate_livechat/optional/specific_department');
        $this->_settings['specific_route_id'] = Mage::getStoreConfig('ultimate_livechat/optional/specific_route_id');
        $this->_settings['hosted_mode_uri_override'] = Mage::getStoreConfig('ultimate_livechat/advanced/hosted_mode_uri_override');
        $this->_settings['title'] = Mage::getStoreConfig('ultimate_livechat/advanced/title');               
        
        return $this->_settings;
    }

    public function get_popup_uri($hostedURI=null, $popupMode=null, $specificOperators=null, $specificDepartment=null, $specificRouteId=null) 
    {
        $popupUri = rtrim($hostedURI, '/');
        $popupUri .= '/index.php?option=com_jlivechat&amp;view=popup&amp;tmpl=component&amp;popup_mode='.$popupMode;

        $activeLanguage = 'en-US';

        if (!empty($activeLanguage)) $popupUri .= '&amp;lang='.$activeLanguage;
        if (!empty($specificOperators)) $popupUri .= '&amp;operators='.$specificOperators;
        if (!empty($specificDepartment)) $popupUri .= '&amp;department='.urlencode($specificDepartment);
        if (!empty($specificRouteId)) $popupUri .= '&amp;routeid='.(int)$specificRouteId;

        return $popupUri;
    }

    public function get_dynamic_image_uri($hostedURI=null, $imgSize=null, $specificOperators=null, $specificDepartment=null, $specificRouteId=null) 
    {
        $imgUri = rtrim($hostedURI, '/');

        if (empty($imgSize)) $imgSize = 'large';

        $imgUri .= '/index.php?option=com_jlivechat&amp;view=popup&amp;task=display_status_img';
        $imgUri .= '&amp;no_html=1&amp;do_not_log=true&amp;size=' . $imgSize;
        $imgUri .= '&amp;t='.time(); // Prevent caching

        if (!empty($specificOperators)) $imgUri .= '&amp;operators='.$specificOperators;
        if (!empty($specificDepartment)) $imgUri .= '&amp;department='.urlencode($specificDepartment);
        if (!empty($specificRouteId)) $imgUri .= '&amp;routeid='.(int)$specificRouteId;

        return $imgUri;
    }

   public function get_tracker_image_uri($hostedURI) 
   {
        $current_user = Mage::getSingleton('customer/session')->getCustomer();

        $trackerUri = rtrim($hostedURI, '/').'/index.php?option=com_jlivechat&amp;no_html=1&amp;tmpl=component';

        $trackerUri .= '&amp;view=popup';
        $trackerUri .= '&amp;task=track_remote_visitor';
        $trackerUri .= '&amp;user_id='.trim($current_user->getId());
        $trackerUri .= '&amp;full_name='.urlencode(trim($current_user->getName()));
        $trackerUri .= '&amp;username='.urlencode(trim($current_user->getName()));
        $trackerUri .= '&amp;email='.urlencode(trim($current_user->getEmail()));
        $trackerUri .= '&amp;last_uri='.urlencode(Mage::helper('core/url')->getCurrentUrl());
        if(isset($_SERVER['HTTP_REFERER'])) $trackerUri .= '&amp;referrer='.urlencode($_SERVER['HTTP_REFERER']);

        return $trackerUri;
    }
}
?>
