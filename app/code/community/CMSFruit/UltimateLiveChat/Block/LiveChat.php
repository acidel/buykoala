<?php
class CMSFruit_UltimateLiveChat_Block_LiveChat extends Mage_Core_Block_Template 
{
    protected $hostedURI;
    
    public function getHostUrl()
    {
        $instance = Mage::helper('ultimate_livechat')->getLiveChatSetting();
        $key = $instance['hosted_mode_api_key'];      
        
        $checkUri = 'https://www.ultimatelivechat.com/index.php?option=com_ultimatelivechat&view=api&format=raw&task=validate_api_key2&k=' . urlencode($key);
        
        $keyDetails = file_get_contents($checkUri);
        $keyDetails = json_decode($keyDetails);

        if($keyDetails->success && isset($keyDetails->user_id) && isset($keyDetails->path)) 
        {  
            $instance['hosted_mode_user_id'] = $keyDetails->user_id;
            $instance['hosted_mode_path'] = $keyDetails->path;
        }

        if(!empty($instance['hosted_mode_uri_override'])) 
        {
            $hostedURI = $instance['hosted_mode_uri_override'];
        } 
        elseif(!empty($instance['hosted_mode_api_key']) && !empty($instance['hosted_mode_user_id']) && !empty($instance['hosted_mode_path'])) 
        {
            if(strtolower($_SERVER['HTTPS']) == 'on') 
            {
                $hostedURI = 'https://';
            } 
            else 
            {
                $hostedURI = 'http://';
            }

            $hostedURI .= 'www.ultimatelivechat.com/sites/' . $instance['hosted_mode_user_id'] . '/' . $instance['hosted_mode_path'] . '/';
        } 
        else 
        {
            $hostedURI = false;
        }
        
        return $hostedURI;
    }

    protected function _prepareLayout() 
    {
        $this->hostedURI = $this->getHostUrl();
        
        if(!empty($this->hostedURI))
        {
            $this->getLayout()->getBlock('head')->addItem('external_css', $this->hostedURI.'components/com_jlivechat/assets/css/jlivechat.min.css');
            $this->getLayout()->getBlock('head')->addItem('external_js', $this->hostedURI.'components/com_jlivechat/js/lazyload-min.js');
            $this->getLayout()->getBlock('head')->addItem('external_js', $this->hostedURI.'components/com_jlivechat/js/jlivechat.min.js');
        }
        
        return parent::_prepareLayout();
    }
}
