<?php
class CMSFruit_UltimateLiveChat_Model_Adminhtml_System_Config_Source_Popup 
{
    protected $_options;

    public function toOptionArray() 
    {
        if(!$this->_options) 
        {
            $this->_options[] = array(
                'label' => Mage::helper('ultimate_livechat')->__('Popup Mode'),
                'value' => 'popup'
            );
            
            $this->_options[] = array(
                'label' => Mage::helper('ultimate_livechat')->__('IFrame Mode'),
                'value' => 'iframe'
            );
        }
        
        return $this->_options;
    }
}
