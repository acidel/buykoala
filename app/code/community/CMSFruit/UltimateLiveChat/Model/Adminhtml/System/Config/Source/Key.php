<?php
class CMSFruit_UltimateLiveChat_Model_Adminhtml_System_Config_Source_Key extends Mage_Core_Model_Config_Data 
{
    protected function _beforeSave() 
    {       
        $key = $this->getValue();
                
        if(!empty($key)) 
        {             
            if(preg_match('/^\*+$/', $this->getValue())) 
            {
                $value = (string)$this->getValue();              
                
                if(!empty($value)) $this->setValue('********************************');
            } 
            else 
            {                
                $checkUri = 'https://www.ultimatelivechat.com/index.php?option=com_ultimatelivechat&view=api&format=raw&task=validate_api_key2&k='.urlencode($key);
                $keyDetails = file_get_contents($checkUri);
                $keyDetails = json_decode($keyDetails);
                
                if($keyDetails->success && isset($keyDetails->user_id) && isset($keyDetails->path)) 
                {             
                    Mage::getConfig()->saveConfig('ultimate_livechat/settings/api_key_valid', $key);     
                                  
                    $value = (string)$this->getValue();
                    
                    if(!empty($value)) $this->setValue('********************************');
                } 
                else 
                {                   
                    $key = '';
                    
                    Mage::getConfig()->saveConfig('ultimate_livechat/settings/api_key_valid', $key);
                    
                    $this->setValue($key);
                    
                    Mage::getSingleton('core/session')->addNotice('Hosted Mode API Access Key is not valid');
                }
            }
        }
        
        return $this;
    }
}
