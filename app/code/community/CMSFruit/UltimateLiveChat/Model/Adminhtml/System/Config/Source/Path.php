<?php
class CMSFruit_UltimateLiveChat_Model_Adminhtml_System_Config_Source_Path extends Mage_Core_Model_Config_Data 
{
    protected function _beforeSave() 
    {
        $value = $this->getValue();
        
        if(!empty($value)) 
        {            
            if(!preg_match('@(/$)@', $value)) $value .= '/';
        }
        
        $this->setValue($value);
        
        return $this;
    }
}
