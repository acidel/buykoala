<?php

class CMSFruit_UltimateLiveChat_Model_Adminhtml_System_Config_Source_Image 
{
    protected $_options;

    public function toOptionArray() 
    {
        if(!$this->_options) 
        {
            $this->_options[] = array(
                'label' => Mage::helper('ultimate_livechat')->__('Large'),
                'value' => 'large'
            );
            
            $this->_options[] = array(
                'label' => Mage::helper('ultimate_livechat')->__('Small'),
                'value' => 'small'
            );
        }
        
        return $this->_options;
    }
}
